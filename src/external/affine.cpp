#include <iostream>
#include <cassert>
#include <map>
#include <set>

#include "numbers/interval.hpp"
#include "numbers/interval_utility.hpp"
#include "numbers/real.hpp"
#include "parser/pattern.hpp"
#include "parser/ast.hpp"
#include "external.hpp"

#include "aa_mpfr.hpp"

// Command line parameter to enable external affine tool
extern bool parameter_affine;

// Statistics

// Number of affine invocations
static int stat_affine = 0;
// Smallest interval returned for particular expression, note, this
// quantity is meaningless when bisection is used   
static std::map<ast_real const *, interval> best_interval;

void
print_affine_stats(std::ostream &os)
{
  os << 
    "  Affine Extension:\n"
    "    " << stat_affine << " affine expressions were evaluated.\n";
  for (std::map<ast_real const *, interval>::iterator i=best_interval.begin(), e=best_interval.end();
       i != e; ++i) {
    os <<
      "    Affine result for: " <<  dump_real(i->first) << " -> " << i->second << "\n";
  }
}            

static
void update_stat_interval(ast_real const * r, interval &iv)
{
  std::map<ast_real const *, interval>::iterator iter = best_interval.find(r);
  if (iter != best_interval.end())
    iter->second = ( iv < iter->second ) ? iv : iter->second;
  else
    best_interval[r] = iv;
}

// Affine Extension              
class AffineExt : public External {
private:
  
  typedef std::set<predicated_real> Needed_set_t;

  // Extension makes extensive use of cacheing in all aspects, caches are
  // implemented with maps

  typedef std::map< ast_real const *, ast_real const* >         AST2AST_map_t;
  typedef std::map< ast_real const *, AA::AAM >                 AST2AA_map_t;
  typedef std::map< ast_real const *, interval >                AST2IV_map_t;
  typedef std::map< const ast_real *, bool >                    AST2bool_map_t;
  typedef std::map< const ast_real *, AffineExt::Needed_set_t>  AST2Needed_map_t;

  AST2AST_map_t     Rnd2Err;  // Map round fns to hypotheses nodes 
  AST2IV_map_t      AST2IV;   // Map AST nodes to enclosures
  AST2Needed_map_t  AST2Dep;  // Map AST to full dependencies

  AST2AA_map_t     AACache;        // Cache of AA expressions  
  AST2bool_map_t   CanCreateCache; // Cache of checked expressions 
  AST2Needed_map_t NeededCache;    // Cache of expression dependencies


  Needed_set_t Needed_Int;  // Internal dependencies

public:
  
  // External API

  interval compute(const ast_real *target, const std::vector<property> &hyps);

  bool canCreateScheme(predicated_real const &target);

  void fillNeededReals(preal_vect &needed, const ast_real *target);
  

private:

  // Helpers for canCreateScheme
  bool canCreateScheme_rec(const ast_real *r);
  bool canCreateScheme_int(const ast_real *r);

  // Helpers for fillNeedsReals
  void fillNeededReals(Needed_set_t &needed, const ast_real *target);

   // Update AST -> interval map for properties, returning true if
  // a property has changed
  //  bool updatePropMap(const std::vector<property> &hyps, bool &change);
  bool updatePropMap(const ast_real *target, bool &change);


  // Main Gappa -> AA translation
  AA::AAM gappa2aa(ast_real const *r, const std::vector<property> &hyps);

  // Gappa interval -> AA translation
  AA::AAM gappa2aa(interval const &ib);
  
  // Gappa rnd expression -> AA translation
  AA::AAM gappa2aa(function_class const *f, ast_real const *r, const std::vector<property> &hyps);

  // AA -> Gappa interval
  interval aa2gappa(const AA::AAM &ae);


  inline const AA::AAM& cacheAA(ast_real const *r, const AA::AAM &aa) {
    return AACache.insert(std::make_pair(r,aa)).first->second;
  }
};

// Create ExternalStar factory for affine error estimation

namespace {

  AffineExt ae;

  // Wrapper to create factory on heap to prevent illegal free
  // operation within Gappa
  class Affine_Factory {
  public:
    Affine_Factory(AffineExt &tool, predicated_real const &p) {
      new ExternalSelect_Factory(tool,p);
    }
  };

} // end of anonymous namespace

// Interface for user instantiation of the affine tool, invoked
// by the parser, return 0 to indicate success
int 
register_user_affine_tool(ast_real const *r) {
  new Affine_Factory(ae,predicated_real(r, PRED_BND));
  return 0;
}


bool 
AffineExt::canCreateScheme_int(const ast_real *r) {
  // Only create scheme for linear expressions, or near
  // linear epxressions
  if (real_op const *p = boost::get< real_op const >(r)) {
    switch (p->type) {
    default:
      return false;
    case UOP_ID:
      return (p->ops[0]) ? canCreateScheme_rec(p->ops[0]) : true;
    case UOP_NEG:
    case UOP_EXP:
    case UOP_LOG:
    case UOP_EXP2:
    case UOP_LOG2:
    case UOP_SQRT:
      return canCreateScheme_rec(p->ops[0]);
    case BOP_ADD:
    case BOP_SUB:
    case BOP_MUL:
    case BOP_DIV:
      return canCreateScheme_rec(p->ops[0]) && canCreateScheme_rec(p->ops[1]);
    case ROP_FUN:
      return canCreateScheme_rec(p->ops[0]);
    }
  } else if (hidden_real const *h = boost::get< hidden_real const >(r)) {
    return canCreateScheme_rec(h->real);
  } else
    return true;
}

bool
AffineExt::canCreateScheme_rec(const ast_real *r) {
  // Check cache for already checked node
  AST2bool_map_t::iterator iter = CanCreateCache.find(r);
  if (iter != CanCreateCache.end()) {
    return iter->second;
  }
  
  bool res = canCreateScheme_int(r);
  
  CanCreateCache[r] = res;
  return res;
}

bool 
AffineExt::canCreateScheme(predicated_real const &target) {
  // Check if external tool enabled
  if (!parameter_affine) return false;

  ast_real const *r = target.real();
  return canCreateScheme_rec(r);
}



void 
AffineExt::fillNeededReals(Needed_set_t &needed, const ast_real *target) {
  // Check cache for already computed "needed reals"
  AST2Needed_map_t::iterator iter = NeededCache.find(target);
  if (iter != NeededCache.end()) {
    needed.insert(iter->second.begin(),iter->second.end());
    return;
  }

  if (real_op const *p = boost::get< real_op const >(target)) {
    switch (p->type) {
    default: break;
    case ROP_FUN: {
      // For rnd(x), we include BND(rnd(x)-x) as a needed real to compute
      // the error using Gappa's full capabilities
      ast_real const *ex = p->ops[0];
      ast_real const *ee = normalize(ast_real(real_op(target, BOP_SUB, ex)));
      Rnd2Err[target] = ee;
      needed.insert(predicated_real(ee, PRED_BND));
    }
    }
    
    typedef ast_real_vect::const_iterator iter_t;
    for(iter_t i = p->ops.begin(), end = p->ops.end(); i != end; ++i) {
      fillNeededReals(needed,*i);
    }
  } else if (hidden_real const *h = boost::get< hidden_real const >(target)) {
    fillNeededReals(needed, h->real);
  } else {
    // Base case for recursion - an identifier or number
    needed.insert(predicated_real(target, PRED_BND));
  }

  // Add computed "needed reals" to cache
  NeededCache[target] = needed;
}

void 
AffineExt::fillNeededReals(preal_vect &needed, const ast_real *target) {
  Needed_set_t needed_set, needed_set_ext;
  fillNeededReals(needed_set,target);

  // Fill immediate dependencies
  const ast_real *r = target;
  while (true) {
    if (real_op const *p = boost::get< real_op const >(target)) {
      typedef ast_real_vect::const_iterator iter_t;
      for(iter_t i = p->ops.begin(), end = p->ops.end(); i != end; ++i) {
	needed_set_ext.insert(predicated_real(*i, PRED_BND));
      }
      break;
    } else if (hidden_real const *h = boost::get< hidden_real const >(r)) {
      r = h->real;
    } else {
      needed_set_ext.insert(predicated_real(r, PRED_BND));
      break;
    }
  }

  AST2Dep[target] = needed_set;

  needed.insert(needed.end(),needed_set_ext.begin(),needed_set_ext.end());
}



// Gappa functions defined elsewhere
interval create_interval(ast_number const *lower, ast_number const *upper, bool widen);

  
AA::AAM
AffineExt::gappa2aa(interval const &ib) {
  number const &lb = lower(ib), &ub = upper(ib);
  if (is_singleton(ib)) {
    return AA::AAM(lb.mpfr_data(), parameter_internal_precision+2);
  } else {
    return AA::AAM(lb.mpfr_data(), ub.mpfr_data(), parameter_internal_precision+2);
  }
}


interval
AffineExt::aa2gappa(const AA::AAM &ae) {
  number_base *l = new number_base, *u = new number_base;
  ae.convert(l->val,u->val);
  return interval(number(l),number(u));
}

AA::AAM
AffineExt::gappa2aa(function_class const *f, ast_real const *r,
		    const std::vector<property> &hyps) {
  // Look up property (proof) generated by Gappa for the rounded expression
  // Such a proof is needed real for this expression, and must exist
  AST2AST_map_t::iterator ee_iter = ae.Rnd2Err.find(r); 
  assert(ee_iter != ae.Rnd2Err.end());
  
  AST2IV_map_t::iterator p = AST2IV.find(ee_iter->second); 
  assert(p != AST2IV.end());

  real_op const *op = boost::get< real_op const >(r); assert(op);
  
  // Recursively create the affine expression
  AA::AAM op_aa = gappa2aa(op->ops[0],hyps); 
  
  // Take the lesser of the rounding errors that Gappa estimates and the
  // error estimated from the affine bound
  std::string tmp;
  interval aa_errest = f->absolute_error_from_exact_bnd(aa2gappa(op_aa),tmp);
  if (p->second < aa_errest) {
    // Add in the error Gappa proved as an affine term
    return gappa2aa(p->second) + op_aa;
  } else {
    // Add in the error we estimated from the affine expression
    return gappa2aa(aa_errest) + op_aa;
  }
}

AA::AAM 
AffineExt::gappa2aa(ast_real const *r, const std::vector<property> &hyps) {
  // Look for previously computed AST nodes
  AST2AA_map_t::iterator i = AACache.find(r);
  if (i != AACache.end())
    return i->second;


  if (real_op const *p = boost::get< real_op const >(r)) {
    switch (p->type) {
    default:
      fprintf(stderr,"Dump w/ Op: %s Type: %d\n",dump_real(r).c_str(),p->type);
      assert(false && "Function not yet supported");
      break;
    case UOP_NEG:
      return cacheAA(r, -gappa2aa(p->ops[0], hyps));
    case UOP_EXP:
      return cacheAA(r, exp(gappa2aa(p->ops[0], hyps)));
    case UOP_LOG:
      return cacheAA(r, log(gappa2aa(p->ops[0], hyps)));
    case UOP_EXP2:
      return cacheAA(r, exp2(gappa2aa(p->ops[0], hyps)));
    case UOP_LOG2:
      return cacheAA(r, log2(gappa2aa(p->ops[0], hyps)));
    case UOP_SQRT:
      return cacheAA(r, sqrt(gappa2aa(p->ops[0], hyps)));
    case BOP_ADD:
      return cacheAA(r, gappa2aa(p->ops[0], hyps) + gappa2aa(p->ops[1], hyps));
    case BOP_SUB:
      return cacheAA(r, gappa2aa(p->ops[0], hyps) - gappa2aa(p->ops[1], hyps));
    case BOP_MUL:
      return cacheAA(r, gappa2aa(p->ops[0], hyps) * gappa2aa(p->ops[1], hyps));
    case BOP_DIV:
      return cacheAA(r, gappa2aa(p->ops[0], hyps) / gappa2aa(p->ops[1], hyps));
    case ROP_FUN:
      return cacheAA(r, gappa2aa(p->fun, r, hyps)); 
    }
  } else if (hidden_real const *h = boost::get< hidden_real const >(r)) {
    return gappa2aa(h->real, hyps);
  } else {
    AST2IV_map_t::iterator res = AST2IV.find(r);
    if (res != AST2IV.end()) {
      return cacheAA(r, gappa2aa(res->second));
    }
 
    if (ast_number const * const *n = boost::get< ast_number const *>(r)) {
      return cacheAA(r, gappa2aa(create_interval((const ast_number*)n,
						 (const ast_number*)n,
						 true)));
    }
    
  }
  
  // Shouldn't ever get here
  assert(false);
  return AA::AAM(parameter_internal_precision+2);
}


bool 
AffineExt::updatePropMap(const ast_real *target, bool &change) {
  change = false;

  AST2Needed_map_t::iterator res = AST2Dep.find(target); assert(res != AST2Dep.end());

  typedef Needed_set_t::iterator iter_t;
  for (iter_t i=res->second.begin(), e=res->second.end(); i!=e; ++i) {
    node *n = find_proof(*i); 
    assert(n);  // Kept for debugging purposes
    if (!n) return false;

    property p = n->get_result();
    
    const ast_real *r = p.real.real();
    AST2IV_map_t::iterator res = AST2IV.find(r);
    if (res == AST2IV.end()) {
      AST2IV[r] = p.bnd();
    } else if (lower(p.bnd()) != lower(res->second) ||
    	       upper(p.bnd()) != upper(res->second)) {
      AST2IV[r] = p.bnd();
      change = true;
    }
  }

  return true;
}


interval 
AffineExt::compute(const ast_real *target, const std::vector<property> &hyps) {
  // We only need these caches during initialization. Can free as soon as we start
  // computing enclosures.
  NeededCache.clear();
  CanCreateCache.clear();

  // Reset the cache of AST to AA exprs if props have changed
  bool change;
  if (!updatePropMap(target, change))
    return interval();
  if (change)
    AACache.clear();

  AA::AAM aa_expr = gappa2aa(target, hyps);
  interval res = aa2gappa(aa_expr);

  // Keep statistics
  ++stat_affine; 
  update_stat_interval(target, res);

  return res;
}
