#include <iostream>

#include "ext_IV.hpp"
using namespace EXT;


bool EXT::is_zero(const IV& iv) { 
  return in_zero(iv) && singleton(iv); 
}

void EXT::iv2mpfr(mpfr_t& lo, mpfr_t& hi, const IV& iv) {
  mpfr_set(lo, iv.lower().getVal(), GMP_RNDD);
  mpfr_set(hi, iv.upper().getVal(), GMP_RNDU);
}

AA::AAM EXT::operator+(const AA::AAM& lhs, const IV &rhs) {
  return lhs + AA::AAM(rhs.lower().getVal(),rhs.upper().getVal(), 
		       mpfr_get_prec(rhs.lower().getVal()));
}

std::ostream& EXT::operator<<(std::ostream& os, const IV& iv) {
  os << "[" << iv.lower() << "," << iv.upper() << "]";
  return os;
}


