#include <cassert>

#include "aa_mpfr.hpp"
using namespace AA;

AAM::index_t AAM::LastIdx = 0;

AAM::AAM(int prec) : Prec(prec) {
  mpfr_init2(Center,Prec);
}

AAM::AAM(const AAM &aam) : Prec(aam.Prec) {
  mpfr_init2(Center,Prec);
  mpfr_set(Center, aam.Center, GMP_RNDN);
  
  Coeffs.reserve(aam.numCoeffs());
  for (coeff_const_iterator i=aam.Coeffs.begin(), e=aam.Coeffs.end(); i!=e; ++i) {
    if (mpfr_zero_p(&(i->second))) continue;
    else this->push_back(i);
  }
}

AAM::AAM(const mpfr_t &center, int prec) : Prec(prec) {
  mpfr_init2(Center, Prec);
  mpfr_set(Center, center, GMP_RNDN);
}

AAM::AAM(const mpfr_t &lower, const mpfr_t &upper, int prec) : Prec(prec) {
  index_t ci = incLast();
  mpfr_init2(Center,Prec);

  MPFR_DECL_INIT(tmp1,Prec); MPFR_DECL_INIT(tmp2,Prec);
  
  // Center
  mpfr_div_ui(tmp1,lower,2,GMP_RNDN);
  mpfr_div_ui(tmp2,upper,2,GMP_RNDN);
  mpfr_add(Center,tmp1,tmp2,GMP_RNDN);

  // Span
  mpfr_sub(tmp1,upper,Center,GMP_RNDU);
  mpfr_sub(tmp2,Center,lower,GMP_RNDU);
  mpfr_max(tmp1, tmp1, tmp2, GMP_RNDN);
  this->push_back(incLast(),tmp1);
}

AAM::AAM(const AAM &x, const mpfr_t &alpha, const mpfr_t &beta, const mpfr_t &delta) : Prec(x.Prec) {
  mpfr_init2(Center,Prec);
  mpfr_fma(Center,alpha,x.Center,beta,GMP_RNDN);

  Coeffs.reserve(x.numCoeffs());
  for (coeff_const_iterator i=x.Coeffs.begin(), e=x.Coeffs.end(); i!=e; ++i) {
    if (mpfr_zero_p(&(i->second))) continue;
    else {
      mpfr_ptr e = r2m(this->push_back(i2idx(i)));
      mpfr_mul(e, i2m(i), alpha, GMP_RNDN);
    }
  }

  mpfr_set(r2m(this->push_back(incLast())),delta,GMP_RNDN);
}

AAM::~AAM() {
  mpfr_clear(Center);
  for (coeff_iterator i=Coeffs.begin(), e=Coeffs.end(); i!=e; ++i)
    mpfr_clear(i2m(i));
}

AAM
AAM::operator-() const {
  AAM tmp(*this);
  mpfr_neg(tmp.Center,tmp.Center,GMP_RNDN);
  for (coeff_iterator i=tmp.Coeffs.begin(), e=tmp.Coeffs.end(); i!=e; ++i)
    mpfr_neg(i2m(i),i2m(i),GMP_RNDN);
  return tmp;
}

AAM&
AAM::operator=(const AAM &rhs) {
  rhs.dump(stderr); fprintf(stderr,"\n");
  Prec = rhs.Prec;
  
  mpfr_set(Center,rhs.Center,GMP_RNDN);
  
  for (coeff_iterator i=Coeffs.begin(), e=Coeffs.end(); i!=e; ++i)
    mpfr_clear(i2m(i));
  Coeffs.clear();

  for (coeff_const_iterator i=rhs.coeff_begin(), e=rhs.coeff_end(); i!=e; ++i) {
    if (mpfr_zero_p(i2m(i))) continue;
    else this->push_back(i);
  }
  return *this;
}

AAM  
AAM::operator + (const AAM &rhs) const {
  AAM tmp(Prec);
  tmp.reserve(this->numCoeffs()+rhs.numCoeffs());

  mpfr_add(tmp.Center,this->Center,rhs.Center, GMP_RNDN);
  
  coeff_const_iterator l_i = this->coeff_begin(), r_i = rhs.coeff_begin();
  while (l_i != this->coeff_end() && r_i != rhs.coeff_end()) {
    if (i2idx(l_i) < i2idx(r_i))
      tmp.push_back(l_i++);
    else if (i2idx(l_i) > i2idx(r_i))
      tmp.push_back(r_i++);
    else {
      mpfr_add(r2m(tmp.push_back(i2idx(l_i))),i2m(l_i),i2m(r_i), GMP_RNDN);
      ++l_i; ++r_i;
    }
  }
  
  while (l_i != this->coeff_end())
    tmp.push_back(l_i++);
  while (r_i != rhs.coeff_end())
    tmp.push_back(r_i++);
  
  return tmp;
}
    
AAM  
AAM::operator - (const AAM &rhs) const {
  AAM tmp(Prec);
  tmp.reserve(this->numCoeffs()+rhs.numCoeffs());
  
  mpfr_sub(tmp.Center,this->Center,rhs.Center, GMP_RNDN);
  
  coeff_const_iterator l_i = this->coeff_begin(), r_i = rhs.coeff_begin();
  while (l_i != this->coeff_end() && r_i != rhs.coeff_end()) {
    if (i2idx(l_i) < i2idx(r_i))
      tmp.push_back(l_i++);
    else if (i2idx(l_i) > i2idx(r_i)) {
      mpfr_neg(r2m(tmp.push_back(r_i)),i2m(r_i),GMP_RNDN);
      ++r_i;
    } else {
      mpfr_sub(r2m(tmp.push_back(i2idx(l_i))),i2m(l_i),i2m(r_i), GMP_RNDN);
      ++l_i; ++r_i;
    }
  }
  
  while (l_i != this->coeff_end())
    tmp.push_back(l_i++);
  while (r_i != rhs.coeff_end()) {
    mpfr_neg(r2m(tmp.push_back(i2idx(r_i))),i2m(r_i),GMP_RNDN);
    ++r_i;
  }
   
  return tmp;
}



AAM  
AAM::operator * (const AAM &rhs) const {
  AAM tmp(Prec);
  tmp.reserve(this->numCoeffs()+rhs.numCoeffs()+1);

  mpfr_mul(tmp.Center,this->Center,rhs.Center, GMP_RNDN);

  coeff_const_iterator l_i = this->coeff_begin(), r_i = rhs.coeff_begin();
  while (l_i != this->coeff_end() && r_i != rhs.coeff_end()) {
    if (i2idx(l_i) < i2idx(r_i)) {
      mpfr_mul(r2m(tmp.push_back(i2idx(l_i))),i2m(l_i),rhs.Center,GMP_RNDN);
      ++l_i;
    } else if (i2idx(l_i) > i2idx(r_i)) {
      mpfr_mul(r2m(tmp.push_back(i2idx(r_i))),i2m(r_i),this->Center,GMP_RNDN);
      ++r_i;
    } else {
      mpfr_ptr v = r2m(tmp.push_back(i2idx(l_i)));
      mpfr_mul(v,i2m(l_i),rhs.Center,GMP_RNDN);
      mpfr_fma(v,i2m(r_i),this->Center,v,GMP_RNDN);
      ++l_i; ++r_i;
    }
  }

  while (l_i != this->coeff_end()) {
    mpfr_mul(r2m(tmp.push_back(i2idx(l_i))),i2m(l_i),rhs.Center,GMP_RNDN);
    ++l_i;
  } 
  while (r_i != rhs.coeff_end()) {
    mpfr_mul(r2m(tmp.push_back(i2idx(r_i))),i2m(r_i),this->Center,GMP_RNDN);
    ++r_i;
  }

  // Create new coeffecient for 2nd order terms
  MPFR_DECL_INIT(tmpl,Prec);
  MPFR_DECL_INIT(tmpr,Prec);
  this->radius(tmpl); rhs.radius(tmpr);

  mpfr_mul(r2m(tmp.push_back(incLast())),tmpl,tmpr,GMP_RNDN);
  
  return tmp;
}

AAM
AAM::operator / (const AAM &rhs) const {
  return this->operator*(inv(rhs));
}

AAM
AA::inv(const AAM &x) {
  // Convert to interval
  MPFR_DECL_INIT(x_lo, x.Prec);
  MPFR_DECL_INIT(x_hi, x.Prec);
  x.convert(x_lo, x_hi);

  // Check for contained zero
  assert(mpfr_nan_p(x_lo) == 0 && mpfr_nan_p(x_hi) == 0);
  if (mpfr_cmp_si(x_lo,0) <= 0 && mpfr_cmp_si(x_hi,0) >= 0) {
    MPFR_DECL_INIT(ret, x.Prec);
    mpfr_set_inf(ret, 1); // Set to "positive" infinity
    return AAM(ret, x.Prec);
  }

  // Compute the MinRange.inv approximation
  mpfr_abs(x_lo, x_lo, GMP_RNDN);
  mpfr_abs(x_hi, x_hi, GMP_RNDN);

  MPFR_DECL_INIT(a, x.Prec); MPFR_DECL_INIT(b, x.Prec);
  mpfr_min(a, x_lo, x_hi, GMP_RNDN);
  mpfr_max(b, x_lo, x_hi, GMP_RNDN);

  MPFR_DECL_INIT(alpha, x.Prec); MPFR_DECL_INIT(beta, x.Prec); MPFR_DECL_INIT(delta, x.Prec);
  
  MPFR_DECL_INIT(tmp1, x.Prec);
  MPFR_DECL_INIT(dmax, x.Prec); MPFR_DECL_INIT(dmin, x.Prec);
  
  mpfr_mul(alpha, b, b, GMP_RNDU);   // alpha = - DOWN(1/b^2)
  mpfr_si_div(alpha, 1, alpha, GMP_RNDD);
  mpfr_neg(alpha, alpha, GMP_RNDN);

  mpfr_si_div(dmax, 1, a, GMP_RNDU); // dmax = UP(1/a - alpha*a)
  mpfr_mul(tmp1, alpha, a, GMP_RNDD);
  mpfr_sub(dmax, dmax, tmp1, GMP_RNDU);

  mpfr_si_div(dmin, 1, b, GMP_RNDD); // dmin = DOWN(1/b - alpha*b)
  mpfr_mul(tmp1, alpha, b, GMP_RNDD);
  mpfr_sub(dmin, dmin, tmp1, GMP_RNDD);

  // beta = mid(dmin,dmax), delta = rad(dmin,dmax)
  AAM::mid_rad(beta, delta, dmin, dmax, x.Prec);
  if (mpfr_cmp_si(x_lo,0) < 0)
    mpfr_neg(beta, beta, GMP_RNDN);

  return AAM(x, alpha, beta, delta);
}

AAM 
AA::exp(const AAM &x) {
  // Convert to interval
  MPFR_DECL_INIT(x_lo, x.Prec);
  MPFR_DECL_INIT(x_hi, x.Prec);
  x.convert(x_lo, x_hi);

  // Check for problem inputs
  assert(mpfr_nan_p(x_lo) == 0 && mpfr_nan_p(x_hi) == 0);
  if (mpfr_inf_p(x_hi)) {
    MPFR_DECL_INIT(ret, x.Prec);
    mpfr_set_inf(ret, 1); // Set to "positive" infinity
    return AAM(ret, x.Prec);
  }

  // Compute the MinRange.exp approximation
  // Note: We use MinRange instead of Chebyshev to prevent getting a negative result
  // when the input range is large.
  MPFR_DECL_INIT(ea, x.Prec); MPFR_DECL_INIT(eb, x.Prec);
  MPFR_DECL_INIT(dmin, x.Prec); MPFR_DECL_INIT(dmax, x.Prec);
  MPFR_DECL_INIT(alpha, x.Prec); MPFR_DECL_INIT(beta, x.Prec); MPFR_DECL_INIT(delta, x.Prec);

  mpfr_exp(ea, x_lo, GMP_RNDD);
  mpfr_exp(eb, x_hi, GMP_RNDU);
  mpfr_set(alpha, ea, GMP_RNDN);

  if (mpfr_zero_p(alpha)) {
    mpfr_set(dmax, eb, GMP_RNDN);
    mpfr_set_si(dmin, 0, GMP_RNDN);
    
  } else {
    mpfr_mul(dmax, alpha, x_hi, GMP_RNDD); // dmax = UP(eb-alpha*x_hi)
    mpfr_sub(dmax, eb, dmax, GMP_RNDU);
    
    mpfr_mul(dmin, alpha, x_lo, GMP_RNDU); // dmin = UP(ea-alpha*x_lo)
    mpfr_sub(dmin, ea, dmin, GMP_RNDD);
  } 

  MPFR_DECL_INIT(tmp1, x.Prec); MPFR_DECL_INIT(tmp2, x.Prec);

  mpfr_div_si(tmp1,dmin,2,GMP_RNDN); // beta = mid(dmin,dmax)
  mpfr_div_si(tmp2,dmax,2,GMP_RNDN);
  mpfr_add(beta,tmp1,tmp2,GMP_RNDN);

  mpfr_sub(tmp1,dmax,beta,GMP_RNDU); // delta = rad(dmin,dmax)
  mpfr_sub(tmp2,beta,dmin,GMP_RNDU);
  mpfr_max(delta,tmp1,tmp2,GMP_RNDN);

  return AAM(x, alpha, beta, delta);
}

AAM
AA::log(const AAM &x) {
  // Convert to interval
  MPFR_DECL_INIT(x_lo, x.Prec);
  MPFR_DECL_INIT(x_hi, x.Prec);
  x.convert(x_lo, x_hi);

  // Check for problem inputs
  assert(mpfr_nan_p(x_lo) == 0 && mpfr_nan_p(x_hi) == 0);
  assert(mpfr_cmp_si(x_lo, 0) >= 0 && mpfr_cmp_si(x_hi, 0) >= 0);
  if (mpfr_zero_p(x_lo)) {
    MPFR_DECL_INIT(ret, x.Prec);
    mpfr_set_inf(ret, -1); // Set to "negative" infinity
    return AAM(ret, x.Prec);
  }
  if (mpfr_inf_p(x_hi)) {
    MPFR_DECL_INIT(ret, x.Prec);
    mpfr_set_inf(ret, 1); // Set to "positive" infinity
    return AAM(ret, x.Prec);
  }

  // Compute the MinRange.log approximation
  // Note: We use MinRange instead of Chebyshev to prevent getting out of range
  // results
  MPFR_DECL_INIT(la, x.Prec); MPFR_DECL_INIT(lb, x.Prec);
  MPFR_DECL_INIT(dmin, x.Prec); MPFR_DECL_INIT(dmax, x.Prec); 
  MPFR_DECL_INIT(alpha, x.Prec); MPFR_DECL_INIT(beta, x.Prec); MPFR_DECL_INIT(delta, x.Prec);

  mpfr_log(la, x_lo, GMP_RNDD);
  mpfr_log(lb, x_hi, GMP_RNDU);
  mpfr_si_div(alpha, 1, x_hi, GMP_RNDD);  // alpha = DOWN(1/b)

  mpfr_mul(dmax, alpha, x_hi, GMP_RNDD);  // dmax = UP(lb-alpha*x_hi)
  mpfr_sub(dmax, lb, dmax, GMP_RNDU);  

  mpfr_mul(dmin, alpha, x_lo, GMP_RNDU);  // dmin = DOWN(la-alpha*x_lo)
  mpfr_sub(dmin, la, dmin, GMP_RNDD);
  
  MPFR_DECL_INIT(tmp1, x.Prec); MPFR_DECL_INIT(tmp2, x.Prec);

  mpfr_div_si(tmp1,dmin,2,GMP_RNDN); // beta = mid(dmin,dmax)
  mpfr_div_si(tmp2,dmax,2,GMP_RNDN);
  mpfr_add(beta,tmp1,tmp2,GMP_RNDN);

  mpfr_sub(tmp1,dmax,beta,GMP_RNDU); // delta = rad(dmin,dmax)
  mpfr_sub(tmp2,beta,dmin,GMP_RNDU);
  mpfr_max(delta,tmp1,tmp2,GMP_RNDN);

  return AAM(x, alpha, beta, delta);
}

AAM 
AA::exp2(const AAM &x) {
  // Convert to interval
  MPFR_DECL_INIT(x_lo, x.Prec);
  MPFR_DECL_INIT(x_hi, x.Prec);
  x.convert(x_lo, x_hi);

  // Check for problem inputs
  assert(mpfr_nan_p(x_lo) == 0 && mpfr_nan_p(x_hi) == 0);
  if (mpfr_inf_p(x_hi)) {
    MPFR_DECL_INIT(ret, x.Prec);
    mpfr_set_inf(ret, 1); // Set to "positive" infinity
    return AAM(ret, x.Prec);
  }

  // Compute the MinRange.exp approximation
  // Note: We use MinRange instead of Chebyshev to prevent getting a negative result
  // when the input range is large.
  MPFR_DECL_INIT(ea, x.Prec); MPFR_DECL_INIT(eb, x.Prec);
  MPFR_DECL_INIT(dmin, x.Prec); MPFR_DECL_INIT(dmax, x.Prec);
  MPFR_DECL_INIT(alpha, x.Prec); MPFR_DECL_INIT(beta, x.Prec); MPFR_DECL_INIT(delta, x.Prec);

  mpfr_exp2(ea, x_lo, GMP_RNDD);
  mpfr_exp2(eb, x_hi, GMP_RNDU);
  mpfr_set(alpha, ea, GMP_RNDN);

  if (mpfr_zero_p(alpha)) {
    mpfr_set(dmax, eb, GMP_RNDN);
    mpfr_set_si(dmin, 0, GMP_RNDN);
    
  } else {
    mpfr_mul(dmax, alpha, x_hi, GMP_RNDD); // dmax = UP(eb-alpha*x_hi)
    mpfr_sub(dmax, eb, dmax, GMP_RNDU);
    
    mpfr_mul(dmin, alpha, x_lo, GMP_RNDU); // dmin = UP(ea-alpha*x_lo)
    mpfr_sub(dmin, ea, dmin, GMP_RNDD);
  } 

  MPFR_DECL_INIT(tmp1, x.Prec); MPFR_DECL_INIT(tmp2, x.Prec);

  mpfr_div_si(tmp1,dmin,2,GMP_RNDN); // beta = mid(dmin,dmax)
  mpfr_div_si(tmp2,dmax,2,GMP_RNDN);
  mpfr_add(beta,tmp1,tmp2,GMP_RNDN);

  mpfr_sub(tmp1,dmax,beta,GMP_RNDU); // delta = rad(dmin,dmax)
  mpfr_sub(tmp2,beta,dmin,GMP_RNDU);
  mpfr_max(delta,tmp1,tmp2,GMP_RNDN);

  return AAM(x, alpha, beta, delta);
}

AAM
AA::log2(const AAM &x) {
  // Convert to interval
  MPFR_DECL_INIT(x_lo, x.Prec);
  MPFR_DECL_INIT(x_hi, x.Prec);
  x.convert(x_lo, x_hi);

  // Check for problem inputs
  assert(mpfr_nan_p(x_lo) == 0 && mpfr_nan_p(x_hi) == 0);
  assert(mpfr_cmp_si(x_lo, 0) >= 0 && mpfr_cmp_si(x_hi, 0) >= 0);
  if (mpfr_zero_p(x_lo)) {
    MPFR_DECL_INIT(ret, x.Prec);
    mpfr_set_inf(ret, -1); // Set to "negative" infinity
    return AAM(ret, x.Prec);
  }
  if (mpfr_inf_p(x_hi)) {
    MPFR_DECL_INIT(ret, x.Prec);
    mpfr_set_inf(ret, 1); // Set to "positive" infinity
    return AAM(ret, x.Prec);
  }

  // Compute the MinRange.log approximation
  // Note: We use MinRange instead of Chebyshev to prevent getting out of range
  // results
  MPFR_DECL_INIT(la, x.Prec); MPFR_DECL_INIT(lb, x.Prec);
  MPFR_DECL_INIT(dmin, x.Prec); MPFR_DECL_INIT(dmax, x.Prec); 
  MPFR_DECL_INIT(alpha, x.Prec); MPFR_DECL_INIT(beta, x.Prec); MPFR_DECL_INIT(delta, x.Prec);

  mpfr_log2(la, x_lo, GMP_RNDD);
  mpfr_log2(lb, x_hi, GMP_RNDU);
  mpfr_si_div(alpha, 1, x_hi, GMP_RNDD);  // alpha = DOWN(1/b)

  mpfr_mul(dmax, alpha, x_hi, GMP_RNDD);  // dmax = UP(lb-alpha*x_hi)
  mpfr_sub(dmax, lb, dmax, GMP_RNDU);  

  mpfr_mul(dmin, alpha, x_lo, GMP_RNDU);  // dmin = DOWN(la-alpha*x_lo)
  mpfr_sub(dmin, la, dmin, GMP_RNDD);
  
  MPFR_DECL_INIT(tmp1, x.Prec); MPFR_DECL_INIT(tmp2, x.Prec);

  mpfr_div_si(tmp1,dmin,2,GMP_RNDN); // beta = mid(dmin,dmax)
  mpfr_div_si(tmp2,dmax,2,GMP_RNDN);
  mpfr_add(beta,tmp1,tmp2,GMP_RNDN);

  mpfr_sub(tmp1,dmax,beta,GMP_RNDU); // delta = rad(dmin,dmax)
  mpfr_sub(tmp2,beta,dmin,GMP_RNDU);
  mpfr_max(delta,tmp1,tmp2,GMP_RNDN);

  return AAM(x, alpha, beta, delta);
}


AAM
AA::sqrt(const AAM &x) {
  // Convert to interval
  MPFR_DECL_INIT(x_lo, x.Prec);
  MPFR_DECL_INIT(x_hi, x.Prec);
  x.convert(x_lo, x_hi);

  // Check for problem inputs
  assert(mpfr_nan_p(x_lo) == 0 && mpfr_nan_p(x_hi) == 0);
  assert(mpfr_cmp_si(x_lo, 0) >= 0 && mpfr_cmp_si(x_hi, 0) >= 0);

  MPFR_DECL_INIT(ra, x.Prec); MPFR_DECL_INIT(rb, x.Prec);
  MPFR_DECL_INIT(dmin, x.Prec); MPFR_DECL_INIT(dmax, x.Prec);
  MPFR_DECL_INIT(alpha, x.Prec); MPFR_DECL_INIT(beta, x.Prec); MPFR_DECL_INIT(delta, x.Prec);

  mpfr_sqrt(ra, x_lo, GMP_RNDD); // ra = DOWN(sqrt(x_lo))
  mpfr_sqrt(rb, x_hi, GMP_RNDU); // rb = UP(sqrt(x_hi))
	
  mpfr_mul_si(alpha, rb, 2, GMP_RNDU); // alpha = UP(2*rb)
  
  MPFR_DECL_INIT(tmp1, x.Prec); MPFR_DECL_INIT(tmp2, x.Prec);

  mpfr_div(tmp1, ra, alpha, GMP_RNDU); // dmin = DOWN(ra*(1-ra/alpha))
  mpfr_si_sub(tmp2, 1, tmp1, GMP_RNDD);
  mpfr_mul(dmin, ra, tmp2, GMP_RNDD);
  
  mpfr_div(tmp1, rb, alpha, GMP_RNDU); // dmax = UP(rb*(1-rb/alpha))
  mpfr_si_sub(tmp2, 1, tmp1, GMP_RNDD);
  mpfr_mul(dmax, rb, tmp2, GMP_RNDU);

  // beta = mid(dmin,dmax), delta = rad(dmin,dmax)
  AAM::mid_rad(beta, delta, dmin, dmax, x.Prec);
  return AAM(x, alpha, beta, delta);
}


void
AAM::convert(mpfr_t &lower, mpfr_t &upper) const {
  MPFR_DECL_INIT(acc,Prec);
  this->radius(acc);
  mpfr_sub(lower,Center,acc,GMP_RNDD);
  mpfr_add(upper,Center,acc,GMP_RNDU);
}


void
AAM::dump(FILE *fh) const {
  mpfr_out_str(fh, 10, 5, Center, GMP_RNDN);
  for (coeff_const_iterator i=Coeffs.begin(), e=Coeffs.end(); i!=e; ++i) {
    fprintf(fh," + ");
    mpfr_out_str(fh, 10, 5, i2m(i), GMP_RNDN);
    fprintf(fh,"*e%u",i->first);
  }
}

void 
AAM::radius(mpfr_t rad) const {
  mpfr_set_ui(rad,0,GMP_RNDN);
  MPFR_DECL_INIT(tmp,Prec);
  for (coeff_const_iterator i=Coeffs.begin(), e=Coeffs.end(); i!=e; ++i) {
    mpfr_abs(tmp, i2m(i), GMP_RNDN);
    mpfr_add(rad, rad, tmp, GMP_RNDU);
  }
}

void 
AAM::mid_rad(mpfr_t mid, mpfr_t rad, mpfr_t lo, mpfr_t hi, int prec) {
  MPFR_DECL_INIT(tmp1, prec); MPFR_DECL_INIT(tmp2, prec);

  mpfr_div_si(tmp1,lo,2,GMP_RNDN); 
  mpfr_div_si(tmp2,hi,2,GMP_RNDN);
  mpfr_add(mid,tmp1,tmp2,GMP_RNDN);

  mpfr_sub(tmp1,hi,mid,GMP_RNDU); 
  mpfr_sub(tmp2,mid,lo,GMP_RNDU);
  mpfr_max(rad,tmp1,tmp2,GMP_RNDN);
}
