#ifndef GAPPA_EXTERNAL_HRREWRITING_H
#define GAPPA_EXTERNAL_HRREWRITING_H

#include <map>
#include <boost/function.hpp>

#include "hr_AST.hpp"

/* ------------------------------------------------------------
 * Rewriting Support for Hybrid AST
 *
 * Two Components:
 * 1) Tree pattern matching tools
 * 2) Expression Rewriter to apply rewrite rules to AST
 * ------------------------------------------------------------*/


namespace EXT {
  namespace HR {

    namespace RW {

      /* ------------------------------------------------------------
       * Tree Pattern Matching
       *
       * Pattern matching using expression templates. Pattern syntax
       * based on Mathematica. Implementation based on boost Spirit parser.
       *
       * Syntax:
       * rule<> rule_name = some combination of ...
       *
       * _<Node Type>(Optional Predicate Function)  : Match node of Node Type
       * _n<Node Type>(Optional Predicate Function) : Match node and save in slot n
       * rule [ rule, ... ]                         : Match parent/child
       * ------------------------------------------------------------*/
      
      class match_result;

      typedef Expr(*Ptr2RewriteFunc_t)(const match_result&);


      // Result of match
      // Evaluates to "true" if match succeeded, "false" otherwise
      class match_result {
      private:
	bool              Valid_d;
	
	typedef std::map<unsigned int, Expr::Ptr_t> Capture_t;
	Capture_t         Matches_d;
	
	Ptr2RewriteFunc_t Rewrite_d;

      public:
	match_result() : Valid_d(false), Rewrite_d(NULL) {}
	match_result(const match_result& rhs) : 
	  Valid_d(rhs.Valid_d), Matches_d(rhs.Matches_d), Rewrite_d(rhs.Rewrite_d) {}
	match_result(bool valid) : Valid_d(valid), Rewrite_d(NULL) {}
	match_result(unsigned int idx, Expr::Ptr_t ptr) : Valid_d(true), Rewrite_d(NULL) { 
	  Matches_d[idx] = ptr; 
	}

	void setRWFunc(const Ptr2RewriteFunc_t& ptr) { Rewrite_d = ptr; }
	Ptr2RewriteFunc_t getRWFunc() const { return Rewrite_d; }

	operator bool() { return Valid_d; }
	
	Expr operator()(const match_result& mr) { 
	  assert(Valid_d && Rewrite_d); 
	  return (*Rewrite_d)(mr);
	}

	match_result operator+(const match_result& rhs) const {
	  if (!Valid_d || !rhs.Valid_d) return match_result();
	 
	  {
	    match_result res(*this);
	    typedef Capture_t::const_iterator iter_t;
	    for (iter_t i=rhs.Matches_d.begin(), e=rhs.Matches_d.end(); i!=e; ++i) {
	      std::pair<Capture_t::iterator,bool> ins = res.Matches_d.insert(*i);
	      if (!ins.second && (ins.first->second != i->second))
		return match_result();
	    }
	    return res;
	  }
	}

	Expr::Ptr_t operator[](unsigned int i) const {
	  Capture_t::const_iterator iter = Matches_d.find(i);
	  return (iter != Matches_d.end()) ? iter->second : Expr::Ptr_t();
	}
	
	template<typename T>
	T* get(unsigned int i) const {
	  Capture_t::const_iterator iter = Matches_d.find(i);
	  return (iter != Matches_d.end()) ? ExprValue::dyn_cast<T>(iter->second) : NULL;
	}
	
      };
      
      /* ------------------------------------------------------------
       * Expression Template Internals
       * ------------------------------------------------------------*/

      // Relevant classes include template parameter to control the conversion of 
      // "multi-edges" Exprs to single edges

      // Abstract base of all matchers
      struct abstract_matcher {
	virtual match_result match(ExprValue::const_child_iterator b, ExprValue::const_child_iterator e) = 0;
      };

      template<typename T>
      struct matcher : abstract_matcher {
	T M_d;
	
	matcher(T m) : M_d(m) {}
	 
	match_result match(ExprValue::const_child_iterator b, ExprValue::const_child_iterator e) {
	  return M_d.match(b, e);
	}
      };

      // Comma seperated children (Can not be directly assigned to a rule)
      template<typename Base_t, typename Next_t>
      struct children {
	Base_t Base_d;
	Next_t Next_d;

	children() : Base_d(), Next_d() {}
	children(const children<Base_t,Next_t>& rhs) : Base_d(rhs.Base_d), Next_d(rhs.Next_d) {}
	children(const Base_t& base, const Next_t& next) : Base_d(base), Next_d(next) {}

	match_result match(ExprValue::const_child_iterator b, ExprValue::const_child_iterator e) {
	  return (b != e) ? Base_d.match(b,b+1) + Next_d.match(b+1,e) : match_result();
	}
      };

      template<typename Base_t, typename Next_t>
      matcher<children<Base_t,Next_t> > operator,(const Base_t& base, const Next_t& next) {
	return matcher<children<Base_t,Next_t> >(children<Base_t,Next_t>(base, next));
      }

      // Parent Child Relationship
      template<typename Base_t, typename Arg_t, class A=typename Expr::newest>
      struct child : abstract_matcher {
	Base_t Base_d;
	Arg_t  Arg_d;
	
	child() : Base_d(), Arg_d() {}
	child(const child<Base_t,Arg_t,A>& rhs) : Base_d(rhs.Base_d), Arg_d(rhs.Arg_d) {}
	child(const Base_t& base, const Arg_t& arg) : Base_d(base), Arg_d(arg) {}
	
	match_result match(ExprValue::const_child_iterator b, ExprValue::const_child_iterator e) {
	  if (b != e) {
	    return Base_d.match(b,e) + Arg_d.match(A::edge(*b)->child_begin(),
						   A::edge(*b)->child_end() );
	  } else
	    return match_result();
	}
      };

      // Or Relationship
      template<typename LHS_t, typename RHS_t>
      struct or_operator : abstract_matcher {
	LHS_t LHS_d;
	RHS_t RHS_d;

	or_operator() : LHS_d(), RHS_d() {}
	or_operator(const or_operator<LHS_t,RHS_t>& rhs) : LHS_d(rhs.LHS_d), RHS_d(rhs.RHS_d) {}
	or_operator(const LHS_t& lhs, const RHS_t& rhs) : LHS_d(lhs), RHS_d(rhs) {}

	match_result match(ExprValue::const_child_iterator b, ExprValue::const_child_iterator e) {
	  match_result lhs_mr = LHS_d.match(b,e);
	  return ((bool)lhs_mr) ? lhs_mr : RHS_d.match(b,e);
	}
      };

      template<typename LHS_t, typename RHS_t>
      matcher<or_operator<LHS_t,RHS_t> > operator|(const LHS_t& lhs, const RHS_t& rhs) {
	return matcher<or_operator<LHS_t,RHS_t> >(or_operator<LHS_t,RHS_t>(lhs,rhs));
      }

      // Non-capturing Node Matcher
      template<typename N, class A=typename Expr::newest>
      struct _ : abstract_matcher {
	typedef boost::function<match_result (const Expr::Ptr_t&)> PredFun_t;
	PredFun_t Pred_d;

	static inline match_result _default(const Expr::Ptr_t& arg) { return match_result(true); }

	_() : Pred_d(&_default) {}
	_(const _<N,A>& rhs) : Pred_d(rhs.Pred_d) {}
	_(const PredFun_t& pred) : Pred_d(pred) {}

	match_result match(ExprValue::const_child_iterator b, ExprValue::const_child_iterator e) {
	  return ((b != e) && ExprValue::isa<N>(A::edge(*b))) ? Pred_d(A::edge(*b)) : match_result();
	}

	template<typename Arg_t>
	matcher<child<_<N,A>, Arg_t, A> > operator[](const Arg_t& arg) const {
	  return matcher<child<_<N,A>, Arg_t, A> >(child<_<N,A>, Arg_t, A>(*this, arg));
	}

      };

      // Capturing Node Matcher
#define NUMBERED_MATCH( X )						\
      template<typename N, class A=typename Expr::newest>		\
      struct _ ## X : abstract_matcher {				\
	typedef boost::function<match_result (unsigned int, const Expr::Ptr_t&)> PredFun_t; \
	PredFun_t Pred_d;						\
	static inline match_result _default(unsigned int idx, const Expr::Ptr_t& arg) { return match_result(idx,arg); }	\
	_ ## X() : Pred_d(&_default) {}					\
	_ ## X(const _ ## X<N,A>& rhs) : Pred_d(rhs.Pred_d) {}		\
	_ ## X(const PredFun_t& pred) : Pred_d(pred) {}			\
	match_result match(ExprValue::const_child_iterator b, ExprValue::const_child_iterator e) { \
	  return ((b != e) && ExprValue::isa<N>(A::edge(*b))) ? Pred_d(X, A::edge(*b)) : match_result(); \
	}								\
	template<typename Arg_t>					\
	matcher<child<_<N,A>, Arg_t, A> > operator[](const Arg_t& arg) const { \
	  return matcher<child<_<N,A>, Arg_t, A> >(child<_<N,A>, Arg_t, A>(*this, arg)); \
	}								\
      };

      NUMBERED_MATCH( 0 );
      NUMBERED_MATCH( 1 );
      NUMBERED_MATCH( 2 );
      NUMBERED_MATCH( 3 );

#undef NUMBERED_MATCH

      // Filters
      typedef bool(*Ptr2FilterFunc_t)(const match_result&);

      template<typename In_t, typename Filt_t>
      struct filter_fn : abstract_matcher {
	In_t   In_d;
	Filt_t Fn_d;

	filter_fn() : In_d(), Fn_d(NULL) {}
	filter_fn(const filter_fn<In_t,Filt_t>& rhs) : In_d(rhs.In_d), Fn_d(rhs.Fn_d) {}
	filter_fn(const In_t& in, const Filt_t& fn) : In_d(in), Fn_d(fn) {}

	match_result match(ExprValue::const_child_iterator b, ExprValue::const_child_iterator e) {
	  match_result loc = In_d.match(b, e);
	  if (loc && Fn_d(loc)) return loc;
	  else                  return match_result();
	}
      };

      template<typename In_t, typename Filt_t>
      matcher<filter_fn<In_t,Filt_t> > operator>>=(const In_t& in, const Filt_t& fn) {
	return matcher<filter_fn<In_t,Filt_t> >(filter_fn<In_t,Filt_t>(in, fn));
      }

      // Rewrites
      template<typename In_t>
      struct rewrite_fn : abstract_matcher {
	In_t In_d;
	Ptr2RewriteFunc_t Fn_d;

	rewrite_fn() : In_d(), Fn_d(NULL) {}
	rewrite_fn(const rewrite_fn<In_t>& rhs) : In_d(rhs.In_d), Fn_d(rhs.Fn_d) {}
	rewrite_fn(const In_t& in, Ptr2RewriteFunc_t fn) : In_d(in), Fn_d(fn) {}

	match_result match(ExprValue::const_child_iterator b, ExprValue::const_child_iterator e) {
	  match_result loc = In_d.match(b, e);
	  loc.setRWFunc(Fn_d);
	  return loc;
	}
      };

      template<typename In_t >
      matcher<rewrite_fn<In_t> > operator>>(const In_t& in, const Ptr2RewriteFunc_t& fn) {
	return matcher<rewrite_fn<In_t> >(rewrite_fn<In_t>(in, fn));
      }

      // Matching Rule
      template<typename T = abstract_matcher, class A=typename Expr::newest>
      struct rule {
	boost::shared_ptr<T> Ptr_d;

	rule() : Ptr_d() {}
	rule(const rule<T,A>& r) : Ptr_d(r.Ptr_d) {}
	
	template<typename Matcher_t>
	rule(const Matcher_t& m) { Ptr_d.reset(new Matcher_t(m)); }

	template<typename Arg_t>
	matcher<child<rule<T,A>,Arg_t,A> > operator[](const Arg_t& arg) const {
	  return matcher<child<rule<T,A>,Arg_t,A> >(child<rule<T,A>,Arg_t,A>(*this, arg));
	}

	match_result match(ExprValue::const_child_iterator b, ExprValue::const_child_iterator e) {
	  return Ptr_d->match(b, e);
	}
	match_result match(const Expr& expr) {
	  return Ptr_d->match(&expr, &expr+1);
	}
      };


      namespace R {
	
	/* ------------------------------------------------------------
	 * Convenience Rules
	 * ------------------------------------------------------------*/
	
#define BINARY_MATCH( OP )						\
	static inline match_result OP ## _Match(const Expr::Ptr_t& ptr) { \
	  BinaryExpr *be = ExprValue::dyn_cast<BinaryExpr>(ptr.get());	\
	  return (be && be->getOpcode() == BinaryExpr::OP) ? match_result(true) : match_result(); \
	} 
	
	BINARY_MATCH( Mul );
	BINARY_MATCH( Div );
	BINARY_MATCH( Sub );
	BINARY_MATCH( Add );
	
#undef BINARY_MATCH

#define BINARY_CAPTURE( OP )						\
	static inline match_result OP ## _Capture(unsigned int idx, const Expr::Ptr_t& ptr) { \
	  BinaryExpr *be = ExprValue::dyn_cast<BinaryExpr>(ptr.get());	\
	  return (be && be->getOpcode() == BinaryExpr::OP) ? match_result(idx,ptr) : match_result(); \
	} 
	
	BINARY_CAPTURE( Mul );
	BINARY_CAPTURE( Div );
	BINARY_CAPTURE( Sub );
	BINARY_CAPTURE( Add );
	
#undef BINARY_MATCH
	
#define UNARY_MATCH( OP )						\
	static inline match_result OP ## _Match(const Expr::Ptr_t& ptr) { \
	  UnaryExpr *ue = ExprValue::dyn_cast<UnaryExpr>(ptr.get());	\
	  return (ue && ue->getOpcode() == UnaryExpr::OP) ? match_result(true) : match_result(); \
	} 

	UNARY_MATCH( Log );
	UNARY_MATCH( Exp );
	

#undef UNARY_MATCH

#define UNARY_CAPTURE( OP )						\
	static inline match_result OP ## _Capture(unsigned int idx, const Expr::Ptr_t& ptr) { \
	  UnaryExpr *e = ExprValue::dyn_cast<UnaryExpr>(ptr.get());	\
	  return (e && e->getOpcode() == UnaryExpr::OP) ? match_result(idx,ptr) : match_result(); \
	} 
	
	UNARY_CAPTURE( Log );
	UNARY_CAPTURE( Exp );
	
#undef UNARY_MATCH


	// (Non-)Capturing match for ConstantExpr node
	// ---------------------------------------------------------------
	rule<> cnst = _<ConstantExpr>();
	rule<> cnst0 = _0<ConstantExpr>();
	rule<> cnst1 = _1<ConstantExpr>();
	rule<> cnst2 = _2<ConstantExpr>();
	rule<> cnst3 = _3<ConstantExpr>();

	// (Non-)Capturing match for UnaryExpr node
	// ---------------------------------------------------------------
	rule<> log  = _<UnaryExpr>(&Log_Match);
	rule<> log0 = _0<UnaryExpr>(&Log_Capture);
	
	rule<> exp  = _<UnaryExpr>(&Exp_Match);
	rule<> exp0 = _0<UnaryExpr>(&Exp_Capture);

	// (Non-)Capturing match for BinaryExpr node
	// ---------------------------------------------------------------
	rule<> bin  = _<BinaryExpr>();
	rule<> bin0 = _0<BinaryExpr>();
	rule<> bin1 = _1<BinaryExpr>();
	rule<> bin2 = _2<BinaryExpr>();
	rule<> bin3 = _3<BinaryExpr>();

	// (Non-)Capturing match for Add node
	rule<> add  = _<BinaryExpr>(&Add_Match);
	rule<> add0 = _0<BinaryExpr>(&Add_Capture);
	rule<> add1 = _1<BinaryExpr>(&Add_Capture);
	rule<> add2 = _2<BinaryExpr>(&Add_Capture);
	rule<> add3 = _3<BinaryExpr>(&Add_Capture);	

	// (Non-)Capturing match for Add node
	rule<> sub = _<BinaryExpr>(&Sub_Match);
	rule<> sub0 = _0<BinaryExpr>(&Sub_Capture);
	rule<> sub1 = _1<BinaryExpr>(&Sub_Capture);
	rule<> sub2 = _2<BinaryExpr>(&Sub_Capture);
	rule<> sub3 = _3<BinaryExpr>(&Sub_Capture);	

	// (Non-)Capturing match for RoundingExpr node
	// ---------------------------------------------------------------
	rule<> rnd  = _<RoundingExpr>();
	rule<> rnd0 = _0<RoundingExpr>();
	rule<> rnd1 = _1<RoundingExpr>();
	rule<> rnd2 = _2<RoundingExpr>();
	rule<> rnd3 = _3<RoundingExpr>();

	// (Non-)Capturing match for AffineExpr node
	// ---------------------------------------------------------------
	rule<> aff  = _<AffineExpr>();
	rule<> aff0 = _0<AffineExpr>();
	rule<> aff1 = _1<AffineExpr>();
	rule<> aff2 = _2<AffineExpr>();
	rule<> aff3 = _3<AffineExpr>();

	// (Non-)Capturing match for nodes of any type
	// ---------------------------------------------------------------
	rule<> n   = _<ExprValue>();
	rule<> n0  = _0<ExprValue>();
	rule<> n1  = _1<ExprValue>();
	rule<> n2  = _2<ExprValue>();
	rule<> n3  = _3<ExprValue>();

      } // End of R namespace
      
      
      /* ------------------------------------------------------------
       * AST Rewriting
       * ------------------------------------------------------------*/

      // Rewrite Operation
      // Abstract base for rewriting operation
      struct RewriteRule {
	//typedef boost::function<Expr (const Expr&)> Replace_t;
	//
	//// Encapsulate match and return of appropiate rewrite callback function
	//virtual Replace_t operator()(const Expr& expr) = 0;
	virtual rule<>& start() = 0;

	// Select edge from "multi-edge" Exprs
	virtual inline const Expr::Ptr_t& access(const Expr& expr) { 
	  return Expr::newest::edge(expr); 
	}
      };
      
      // Report from Rewrite Operation
      struct RewriteReport {
	Expr::Idx_t StartEdge_d, EndEdge_d;
      };
      
      class ExprRewriter {
      private:
	static void _apply_rewriterule(RewriteRule& rr, Expr& expr) {
	  match_result mr = rr.start().match(expr);
	  Expr::Ptr_t  res = ((bool)(mr) && mr.getRWFunc()) ? 
	    rr.access(expr.push_back(mr(mr))) :
	    rr.access(expr);
	  for (ExprValue::child_iterator i=res->child_begin(), e=res->child_end(); i!=e; ++i)
	    _apply_rewriterule(rr, *i);

	}

      public:
	static RewriteReport expr_rewrite(RewriteRule& rr, Expr& expr) {
	  RewriteReport report = { Expr::curIdx(), Expr::curIdx() };
	  _apply_rewriterule(rr, expr);
	  report.EndEdge_d = Expr::curIdx();
	  return report;
	}
      };
      
      //class ExprRewriter {
      //private:
      //	RewriteRule& Match_d;
      //	
      //	void _apply(Expr& expr) {
      //	  Expr::Ptr_t n;
      //	  // Attempt to apply rewriting rule
      //	  RewriteRule::Replace_t R = Match_d(expr);
      //	  if ((bool)R)
      //	    n = Match_d.access(expr.push_back(R(expr)));
      //	  else
      //	    n = Match_d.access(expr);
      //	  // Iterate through children  recursively applying rule
      //	  for (ExprValue::child_iterator i=n->child_begin(), e=n->child_end(); i!=e; ++i)
      //	    _apply(*i);
      //	}
      //	
      //public:
      //	ExprRewriter(RewriteRule& match) : Match_d(match) {}
      //	
      //	RewriteReport apply(Expr& expr) {
      //	  RewriteReport report = { Expr::curIdx(), Expr::curIdx() };
      //	  _apply(expr);
      //	  report.EndEdge_d = Expr::curIdx();
      //	  return report;
      //	}
      //	
      //};

    } // end of RW namespace

  } // end of HR namespace
} // end of EXT namespace

#endif
