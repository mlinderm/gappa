/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.3"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 1

/* Using locations.  */
#define YYLSP_NEEDED 1



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     IDENT = 258,
     FUNID = 259,
     VARID = 260,
     IN = 261,
     BY = 262,
     NUMBER = 263,
     SQRT = 264,
     LOG = 265,
     EXP = 266,
     LOG2 = 267,
     EXP2 = 268,
     FMA = 269,
     NE = 270,
     LE = 271,
     GE = 272,
     RDIV = 273,
     IMPL = 274,
     OR = 275,
     AND = 276,
     NOT = 277,
     NEG = 278
   };
#endif
/* Tokens.  */
#define IDENT 258
#define FUNID 259
#define VARID 260
#define IN 261
#define BY 262
#define NUMBER 263
#define SQRT 264
#define LOG 265
#define EXP 266
#define LOG2 267
#define EXP2 268
#define FMA 269
#define NE 270
#define LE 271
#define GE 272
#define RDIV 273
#define IMPL 274
#define OR 275
#define AND 276
#define NOT 277
#define NEG 278




/* Copy the first part of user declarations.  */
#line 1 "parser/parser.ypp"

#include <cstdlib>
#include <iostream>

#include "parser/ast.hpp"
#include "proofs/dichotomy.hpp"
#include "parser/parser.h"

#define yyerror(msg) my_yyerror(yylloc, msg)

int yylex(YYSTYPE *yylval_param, YYLTYPE *yylloc_param);

#define error(msg) \
  std::cerr << "Error: " << msg << " at line " << yylloc.first_line << " column " << yylloc.first_column << '\n'
#define warning(msg) \
  std::cerr << "Warning: " << msg << " at line " << yylloc.first_line << " column " << yylloc.first_column << '\n'

int my_yyerror(YYLTYPE &yylloc, char const *errstr) {
    error(errstr);
    return EXIT_FAILURE;
}

#define failure(msg) do {\
  error(msg); \
  YYERROR; \
} while (0)

extern int register_user_external(ast_real const *, ast_ident *);
extern void register_user_rewrite(ast_real const *, ast_real const *, hint_cond_vect *);
extern void generate_graph(ast_prop const *p);
extern void test_ringularity(ast_real const *, ast_real const *, bool);
extern int test_rewriting(ast_real const *, ast_real const *, std::string &);
extern void check_approx(ast_real const *);
extern std::set< ast_real const * > free_variables;
extern bool parameter_rfma;

static ast_number const *negative_number(ast_number const *nn) {
  if (nn->base == 0) return nn;
  ast_number n = *nn;
  char &c = n.mantissa[0];
  assert(c == '+');
  c = '-';
  return normalize(n);
}

static function_class const *rounding_mode = NULL;

static ast_real const *rnd_normalize(ast_real const &real) {
  ast_real const *r = normalize(real);
  if (!rounding_mode) return r;
  return normalize(ast_real(real_op(rounding_mode, r)));
}

static unsigned long read_integer_param(ast_number const *n)
{
  return (unsigned long)(atoi(n->mantissa.c_str()) * 2) | 1;
}

#define YYINCLUDED_STDLIB_H



/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 1
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 69 "parser/parser.ypp"
{
  ast_number const	*number;
  ast_ident		*ident;
  ast_real const	*real;
  ast_real_vect		*reals;
  dichotomy_var		dvar;
  dvar_vect		*dvars;
  ast_prop		*prop;
  function_class const	*function;
  function_params	*params;
  unsigned long		param;
  hint_cond		*precond;
  hint_cond_vect	*preconds;
}
/* Line 193 of yacc.c.  */
#line 219 "parser/parser.cpp"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
} YYLTYPE;
# define yyltype YYLTYPE /* obsolescent; will be withdrawn */
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif


/* Copy the second part of user declarations.  */


/* Line 216 of yacc.c.  */
#line 244 "parser/parser.cpp"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int i)
#else
static int
YYID (i)
    int i;
#endif
{
  return i;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL \
	     && defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss;
  YYSTYPE yyvs;
    YYLTYPE yyls;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE) + sizeof (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  4
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   334

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  44
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  22
/* YYNRULES -- Number of rules.  */
#define YYNRULES  78
/* YYNRULES -- Number of states.  */
#define YYNSTATES  187

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   278

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,    42,     2,     2,     2,
      35,    36,    25,    23,    31,    24,     2,    26,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,    40,
      37,    39,    38,    33,    41,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    30,     2,    32,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    28,    34,    29,    43,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    27
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     6,    11,    15,    23,    27,    31,    41,
      49,    55,    59,    63,    67,    70,    74,    76,    79,    82,
      84,    86,    88,    90,    92,    96,    97,   101,   104,   106,
     109,   110,   116,   123,   126,   129,   133,   137,   139,   141,
     143,   148,   152,   156,   160,   164,   168,   173,   178,   183,
     188,   193,   202,   206,   209,   212,   214,   218,   220,   224,
     226,   230,   236,   238,   242,   246,   250,   254,   258,   262,
     264,   268,   269,   273,   274,   281,   287,   292,   298
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int8 yyrhs[] =
{
      45,     0,    -1,    46,    65,    -1,    56,    28,    47,    29,
      -1,    57,    16,    48,    -1,    57,     6,    30,    48,    31,
      48,    32,    -1,    57,     6,    33,    -1,    57,    17,    48,
      -1,    57,    18,    57,     6,    30,    48,    31,    48,    32,
      -1,    34,    57,    18,    57,    34,    16,     8,    -1,    57,
      18,    57,     6,    33,    -1,    47,    21,    47,    -1,    47,
      20,    47,    -1,    47,    19,    47,    -1,    22,    47,    -1,
      35,    47,    36,    -1,     8,    -1,    23,     8,    -1,    24,
       8,    -1,     8,    -1,    48,    -1,    50,    -1,     3,    -1,
      51,    -1,    52,    31,    51,    -1,    -1,    37,    52,    38,
      -1,     4,    53,    -1,    39,    -1,    54,    39,    -1,    -1,
      56,     3,    55,    57,    40,    -1,    56,    41,     3,    39,
      54,    40,    -1,    56,     5,    -1,    56,     4,    -1,    56,
      41,     5,    -1,    56,    41,     4,    -1,    48,    -1,     5,
      -1,     3,    -1,    54,    35,    58,    36,    -1,    57,    23,
      57,    -1,    57,    24,    57,    -1,    57,    25,    57,    -1,
      57,    26,    57,    -1,    34,    57,    34,    -1,     9,    35,
      57,    36,    -1,    10,    35,    57,    36,    -1,    11,    35,
      57,    36,    -1,    12,    35,    57,    36,    -1,    13,    35,
      57,    36,    -1,    14,    35,    57,    31,    57,    31,    57,
      36,    -1,    35,    57,    36,    -1,    23,    57,    -1,    24,
      57,    -1,    57,    -1,    58,    31,    57,    -1,    48,    -1,
      59,    31,    48,    -1,    57,    -1,    57,     6,    49,    -1,
      57,     6,    35,    59,    36,    -1,    60,    -1,    61,    31,
      60,    -1,    57,    15,    50,    -1,    57,    16,    50,    -1,
      57,    17,    50,    -1,    57,    37,    50,    -1,    57,    38,
      50,    -1,    62,    -1,    63,    31,    62,    -1,    -1,    28,
      63,    29,    -1,    -1,    65,    57,    19,    57,    64,    40,
      -1,    65,    58,    42,    61,    40,    -1,    65,    42,    61,
      40,    -1,    65,    57,    43,    57,    40,    -1,    65,    57,
       7,     3,    40,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   115,   115,   116,   119,   124,   126,   127,   128,   130,
     132,   133,   134,   135,   136,   137,   140,   141,   142,   145,
     152,   159,   160,   163,   164,   172,   173,   176,   189,   190,
     197,   198,   209,   215,   216,   217,   218,   221,   222,   223,
     232,   243,   244,   245,   246,   247,   248,   249,   250,   251,
     252,   253,   260,   261,   262,   265,   266,   273,   274,   277,
     278,   279,   282,   283,   290,   291,   292,   293,   294,   297,
     298,   305,   306,   308,   309,   328,   335,   341,   348
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "IDENT", "FUNID", "VARID", "IN", "BY",
  "NUMBER", "SQRT", "LOG", "EXP", "LOG2", "EXP2", "FMA", "NE", "LE", "GE",
  "RDIV", "IMPL", "OR", "AND", "NOT", "'+'", "'-'", "'*'", "'/'", "NEG",
  "'{'", "'}'", "'['", "','", "']'", "'?'", "'|'", "'('", "')'", "'<'",
  "'>'", "'='", "';'", "'@'", "'$'", "'~'", "$accept", "BLOB", "BLOB1",
  "PROP", "SNUMBER", "INTEGER", "SINTEGER", "FUNCTION_PARAM",
  "FUNCTION_PARAMS_AUX", "FUNCTION_PARAMS", "FUNCTION", "EQUAL", "PROG",
  "REAL", "REALS", "DPOINTS", "DVAR", "DVARS", "PRECOND", "PRECONDS_AUX",
  "PRECONDS", "HINTS", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,    43,    45,    42,    47,   278,   123,   125,
      91,    44,    93,    63,   124,    40,    41,    60,    62,    61,
      59,    64,    36,   126
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    44,    45,    46,    47,    47,    47,    47,    47,    47,
      47,    47,    47,    47,    47,    47,    48,    48,    48,    49,
      50,    51,    51,    52,    52,    53,    53,    54,    55,    55,
      56,    56,    56,    56,    56,    56,    56,    57,    57,    57,
      57,    57,    57,    57,    57,    57,    57,    57,    57,    57,
      57,    57,    57,    57,    57,    58,    58,    59,    59,    60,
      60,    60,    61,    61,    62,    62,    62,    62,    62,    63,
      63,    64,    64,    65,    65,    65,    65,    65,    65
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     2,     4,     3,     7,     3,     3,     9,     7,
       5,     3,     3,     3,     2,     3,     1,     2,     2,     1,
       1,     1,     1,     1,     3,     0,     3,     2,     1,     2,
       0,     5,     6,     2,     2,     3,     3,     1,     1,     1,
       4,     3,     3,     3,     3,     3,     4,     4,     4,     4,
       4,     8,     3,     2,     2,     1,     3,     1,     3,     1,
       3,     5,     1,     3,     3,     3,     3,     3,     3,     1,
       3,     0,     3,     0,     6,     5,     4,     5,     5
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
      30,     0,    73,     0,     1,     2,     0,    34,    33,     0,
       0,    39,    25,    38,    16,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    37,     0,    55,     0,
      28,     0,     0,     0,     0,     0,     0,     0,     0,    36,
      35,     0,    27,     0,     0,     0,     0,     0,     0,    16,
      53,    16,    54,     0,     0,    59,    62,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    29,     0,
      14,     0,     0,     0,     0,     0,     0,     3,     0,     0,
       0,     0,     0,    22,     0,     0,    20,    21,    23,     0,
       0,     0,     0,     0,     0,     0,    45,    52,     0,     0,
      76,    55,     0,     0,    71,    41,    42,    43,    44,     0,
      56,     0,    31,     0,    15,    13,    12,    11,     0,     6,
       4,     7,     0,     0,    17,    18,     0,    26,    46,    47,
      48,    49,    50,     0,    19,     0,    60,    63,    40,    78,
       0,     0,    77,    75,     0,     0,     0,    32,    24,     0,
      57,     0,     0,    69,     0,    74,     0,     0,     0,    10,
       0,     0,    61,     0,     0,     0,     0,     0,    72,     0,
       0,     0,     0,     0,    58,    64,    65,    66,    67,    68,
      70,     9,     5,     0,    51,     0,     8
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     1,     2,    36,    26,   136,    87,    88,    89,    42,
      27,    32,     3,    37,    29,   151,    56,    57,   153,   154,
     141,     5
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -35
static const yytype_int16 yypact[] =
{
     -35,    15,   -35,     9,   -35,   102,    -3,   -35,   -35,   148,
     310,   -35,    -9,   -35,   -35,     0,    40,   110,   111,   119,
     128,   176,   193,   210,   210,   210,   -35,   134,    44,    51,
     -35,    42,   210,   148,   210,   148,   274,    48,   173,   -35,
     -35,    80,   -35,   210,   210,   210,   210,   210,   210,   -35,
     -35,   -35,   -35,   258,     8,    -2,   -35,    46,   210,   170,
     210,   210,   210,   210,   210,   210,   210,   210,   -35,   151,
     -35,   116,   -10,    73,   148,   148,   148,   -35,   202,   218,
     218,   210,   204,   -35,   217,   228,   -35,   -35,   -35,    64,
     142,   223,   230,   237,   244,   281,   -35,   -35,    17,   210,
     -35,   305,    49,   203,   294,    96,    96,   -35,   -35,   169,
     305,   107,   -35,   210,   -35,   313,   229,   -35,   218,   -35,
     -35,   -35,    94,   224,   -35,   -35,    80,   -35,   -35,   -35,
     -35,   -35,   -35,   210,   -35,   218,   -35,   -35,   -35,   -35,
     210,   225,   -35,   -35,   265,   226,   267,   -35,   -35,   285,
     -35,   112,   214,   -35,   -26,   -35,   242,   218,   218,   -35,
     210,   218,   -35,   218,   218,   218,   218,   218,   -35,   210,
     263,   240,   247,   251,   -35,   -35,   -35,   -35,   -35,   -35,
     -35,   -35,   -35,   218,   -35,   253,   -35
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
     -35,   -35,   -35,   -27,   -34,   -35,   160,   153,   -35,   -35,
      -4,   -35,   -35,    -5,   238,   -35,   187,   231,   132,   -35,
     -35,   -35
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1
static const yytype_uint8 yytable[] =
{
      28,    12,    31,   168,    98,   169,    70,    86,    72,    74,
      75,    76,     6,     7,     8,     4,    50,    52,    53,    54,
      55,    61,    62,    63,    64,   134,   114,    69,    41,    71,
      73,    61,    62,    63,    64,    43,    30,     9,    90,    91,
      92,    93,    94,    95,    97,   120,   121,   115,   116,   117,
      10,    59,   135,   101,    78,   104,   105,   106,   107,   108,
     109,   110,    55,    60,    79,    80,    81,    61,    62,    63,
      64,    61,    62,    63,    64,    44,   122,    99,   123,    78,
      66,    68,    66,    83,   145,   138,   100,    65,    14,    79,
      80,    81,    86,    67,    55,   126,    61,    62,    63,    64,
     146,   150,   127,    84,    85,    11,    12,    13,   144,    97,
      14,    15,    16,    17,    18,    19,    20,    61,    62,    63,
      64,    63,    64,   171,   172,    21,    22,   174,   149,    86,
      86,    86,    86,    86,   113,   152,    23,    24,    99,    61,
      62,    63,    64,   161,    25,    45,    46,   143,   162,   185,
      96,    11,    12,    13,    47,   173,    14,    15,    16,    17,
      18,    19,    20,    48,   152,    61,    62,    63,    64,    58,
      33,    21,    22,   103,    61,    62,    63,    64,   128,    11,
      12,    13,    34,    35,    49,    15,    16,    17,    18,    19,
      20,   112,    61,    62,    63,    64,    11,    12,    13,    21,
      22,    51,    15,    16,    17,    18,    19,    20,    12,   142,
      23,    24,    82,    11,    12,    13,    21,    22,    14,    15,
      16,    17,    18,    19,    20,   124,    14,    23,    24,   163,
     164,   165,   118,    21,    22,   119,   125,    61,    62,    63,
      64,    84,    85,   139,    23,    24,    61,    62,    63,    64,
      76,   166,   167,    61,    62,    63,    64,   157,   170,   129,
      61,    62,    63,    64,   147,   155,   130,    61,    62,    63,
      64,   181,   182,   131,    61,    62,    63,    64,   183,   148,
     132,    61,    62,    63,    64,   186,   137,   184,    61,    62,
      63,    64,    96,    74,    75,    76,   102,   158,   111,   156,
     159,   180,     0,    77,    61,    62,    63,    64,    61,    62,
      63,    64,   133,    38,    39,    40,   160,    61,    62,    63,
      64,     0,   140,   175,   176,   177,   178,   179,    61,    62,
      63,    64,    74,    75,    76
};

static const yytype_int16 yycheck[] =
{
       5,     4,     6,    29,     6,    31,    33,    41,    35,    19,
      20,    21,     3,     4,     5,     0,    21,    22,    23,    24,
      25,    23,    24,    25,    26,     8,    36,    32,    37,    34,
      35,    23,    24,    25,    26,    35,    39,    28,    43,    44,
      45,    46,    47,    48,    36,    79,    80,    74,    75,    76,
      41,     7,    35,    58,     6,    60,    61,    62,    63,    64,
      65,    66,    67,    19,    16,    17,    18,    23,    24,    25,
      26,    23,    24,    25,    26,    35,    81,    31,    82,     6,
      31,    39,    31,     3,   118,    36,    40,    43,     8,    16,
      17,    18,   126,    42,    99,    31,    23,    24,    25,    26,
       6,   135,    38,    23,    24,     3,     4,     5,   113,    36,
       8,     9,    10,    11,    12,    13,    14,    23,    24,    25,
      26,    25,    26,   157,   158,    23,    24,   161,   133,   163,
     164,   165,   166,   167,    18,   140,    34,    35,    31,    23,
      24,    25,    26,    31,    42,    35,    35,    40,    36,   183,
      34,     3,     4,     5,    35,   160,     8,     9,    10,    11,
      12,    13,    14,    35,   169,    23,    24,    25,    26,    35,
      22,    23,    24,     3,    23,    24,    25,    26,    36,     3,
       4,     5,    34,    35,     8,     9,    10,    11,    12,    13,
      14,    40,    23,    24,    25,    26,     3,     4,     5,    23,
      24,     8,     9,    10,    11,    12,    13,    14,     4,    40,
      34,    35,    39,     3,     4,     5,    23,    24,     8,     9,
      10,    11,    12,    13,    14,     8,     8,    34,    35,    15,
      16,    17,    30,    23,    24,    33,     8,    23,    24,    25,
      26,    23,    24,    40,    34,    35,    23,    24,    25,    26,
      21,    37,    38,    23,    24,    25,    26,    31,    16,    36,
      23,    24,    25,    26,    40,    40,    36,    23,    24,    25,
      26,     8,    32,    36,    23,    24,    25,    26,    31,   126,
      36,    23,    24,    25,    26,    32,    99,    36,    23,    24,
      25,    26,    34,    19,    20,    21,    58,    30,    67,    34,
      33,   169,    -1,    29,    23,    24,    25,    26,    23,    24,
      25,    26,    31,     3,     4,     5,    31,    23,    24,    25,
      26,    -1,    28,   163,   164,   165,   166,   167,    23,    24,
      25,    26,    19,    20,    21
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    45,    46,    56,     0,    65,     3,     4,     5,    28,
      41,     3,     4,     5,     8,     9,    10,    11,    12,    13,
      14,    23,    24,    34,    35,    42,    48,    54,    57,    58,
      39,    54,    55,    22,    34,    35,    47,    57,     3,     4,
       5,    37,    53,    35,    35,    35,    35,    35,    35,     8,
      57,     8,    57,    57,    57,    57,    60,    61,    35,     7,
      19,    23,    24,    25,    26,    43,    31,    42,    39,    57,
      47,    57,    47,    57,    19,    20,    21,    29,     6,    16,
      17,    18,    39,     3,    23,    24,    48,    50,    51,    52,
      57,    57,    57,    57,    57,    57,    34,    36,     6,    31,
      40,    57,    58,     3,    57,    57,    57,    57,    57,    57,
      57,    61,    40,    18,    36,    47,    47,    47,    30,    33,
      48,    48,    57,    54,     8,     8,    31,    38,    36,    36,
      36,    36,    36,    31,     8,    35,    49,    60,    36,    40,
      28,    64,    40,    40,    57,    48,     6,    40,    51,    57,
      48,    59,    57,    62,    63,    40,    34,    31,    30,    33,
      31,    31,    36,    15,    16,    17,    37,    38,    29,    31,
      16,    48,    48,    57,    48,    50,    50,    50,    50,    50,
      62,     8,    32,    31,    36,    48,    32
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (&yylval, &yylloc, YYLEX_PARAM)
#else
# define YYLEX yylex (&yylval, &yylloc)
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value, Location); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep, yylocationp)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
    YYLTYPE const * const yylocationp;
#endif
{
  if (!yyvaluep)
    return;
  YYUSE (yylocationp);
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep, yylocationp)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
    YYLTYPE const * const yylocationp;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  YY_LOCATION_PRINT (yyoutput, *yylocationp);
  YYFPRINTF (yyoutput, ": ");
  yy_symbol_value_print (yyoutput, yytype, yyvaluep, yylocationp);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *bottom, yytype_int16 *top)
#else
static void
yy_stack_print (bottom, top)
    yytype_int16 *bottom;
    yytype_int16 *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, YYLTYPE *yylsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yylsp, yyrule)
    YYSTYPE *yyvsp;
    YYLTYPE *yylsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      fprintf (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       , &(yylsp[(yyi + 1) - (yynrhs)])		       );
      fprintf (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, yylsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, YYLTYPE *yylocationp)
#else
static void
yydestruct (yymsg, yytype, yyvaluep, yylocationp)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
    YYLTYPE *yylocationp;
#endif
{
  YYUSE (yyvaluep);
  YYUSE (yylocationp);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */






/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
  /* The look-ahead symbol.  */
int yychar;

/* The semantic value of the look-ahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;
/* Location data for the look-ahead symbol.  */
YYLTYPE yylloc;

  int yystate;
  int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int yytoken = 0;
#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  yytype_int16 yyssa[YYINITDEPTH];
  yytype_int16 *yyss = yyssa;
  yytype_int16 *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  YYSTYPE *yyvsp;

  /* The location stack.  */
  YYLTYPE yylsa[YYINITDEPTH];
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;
  /* The locations where the error started and ended.  */
  YYLTYPE yyerror_range[2];

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;
  yylsp = yyls;
#if YYLTYPE_IS_TRIVIAL
  /* Initialize the default location before parsing starts.  */
  yylloc.first_line   = yylloc.last_line   = 1;
  yylloc.first_column = yylloc.last_column = 0;
#endif

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;
	YYLTYPE *yyls1 = yyls;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yyls1, yysize * sizeof (*yylsp),
		    &yystacksize);
	yyls = yyls1;
	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);
	YYSTACK_RELOCATE (yyls);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     look-ahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to look-ahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;
  *++yylsp = yylloc;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location.  */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 3:
#line 116 "parser/parser.ypp"
    { generate_graph((yyvsp[(3) - (4)].prop)); }
    break;

  case 4:
#line 120 "parser/parser.ypp"
    {
    real_op const *p = boost::get< real_op const >((yyvsp[(1) - (3)].real));
    (yyval.prop) = new ast_prop(new ast_atom_bound((yyvsp[(1) - (3)].real), (!p || p->type != UOP_ABS) ? NULL : token_zero, (yyvsp[(3) - (3)].number)));
  }
    break;

  case 5:
#line 125 "parser/parser.ypp"
    { (yyval.prop) = new ast_prop(new ast_atom_bound((yyvsp[(1) - (7)].real), (yyvsp[(4) - (7)].number), (yyvsp[(6) - (7)].number))); }
    break;

  case 6:
#line 126 "parser/parser.ypp"
    { (yyval.prop) = new ast_prop(new ast_atom_bound((yyvsp[(1) - (3)].real), NULL, NULL)); }
    break;

  case 7:
#line 127 "parser/parser.ypp"
    { (yyval.prop) = new ast_prop(new ast_atom_bound((yyvsp[(1) - (3)].real), (yyvsp[(3) - (3)].number), NULL)); }
    break;

  case 8:
#line 129 "parser/parser.ypp"
    { (yyval.prop) = new ast_prop(new ast_atom_bound((yyvsp[(1) - (9)].real), (yyvsp[(3) - (9)].real), (yyvsp[(6) - (9)].number), (yyvsp[(8) - (9)].number))); }
    break;

  case 9:
#line 131 "parser/parser.ypp"
    { (yyval.prop) = new ast_prop(new ast_atom_bound((yyvsp[(2) - (7)].real), (yyvsp[(4) - (7)].real), negative_number((yyvsp[(7) - (7)].number)), (yyvsp[(7) - (7)].number))); }
    break;

  case 10:
#line 132 "parser/parser.ypp"
    { (yyval.prop) = new ast_prop(new ast_atom_bound((yyvsp[(1) - (5)].real), (yyvsp[(3) - (5)].real), NULL, NULL)); }
    break;

  case 11:
#line 133 "parser/parser.ypp"
    { (yyval.prop) = new ast_prop((yyvsp[(1) - (3)].prop), PROP_AND,  (yyvsp[(3) - (3)].prop)); }
    break;

  case 12:
#line 134 "parser/parser.ypp"
    { (yyval.prop) = new ast_prop((yyvsp[(1) - (3)].prop), PROP_OR,   (yyvsp[(3) - (3)].prop)); }
    break;

  case 13:
#line 135 "parser/parser.ypp"
    { (yyval.prop) = new ast_prop((yyvsp[(1) - (3)].prop), PROP_IMPL, (yyvsp[(3) - (3)].prop)); }
    break;

  case 14:
#line 136 "parser/parser.ypp"
    { (yyval.prop) = new ast_prop((yyvsp[(2) - (2)].prop)); }
    break;

  case 15:
#line 137 "parser/parser.ypp"
    { (yyval.prop) = (yyvsp[(2) - (3)].prop); }
    break;

  case 16:
#line 140 "parser/parser.ypp"
    { (yyval.number) = (yyvsp[(1) - (1)].number); }
    break;

  case 17:
#line 141 "parser/parser.ypp"
    { (yyval.number) = (yyvsp[(2) - (2)].number); }
    break;

  case 18:
#line 142 "parser/parser.ypp"
    { (yyval.number) = negative_number((yyvsp[(2) - (2)].number)); }
    break;

  case 19:
#line 146 "parser/parser.ypp"
    {
    if ((yyvsp[(1) - (1)].number)->exponent != 0 || (yyvsp[(1) - (1)].number)->mantissa.size() >= 7) failure("the number does not look like an integer");
    (yyval.number) = (yyvsp[(1) - (1)].number);
  }
    break;

  case 20:
#line 153 "parser/parser.ypp"
    {
    if ((yyvsp[(1) - (1)].number)->exponent != 0 || (yyvsp[(1) - (1)].number)->mantissa.size() >= 7) failure("the number does not look like an integer");
    (yyval.number) = (yyvsp[(1) - (1)].number);
  }
    break;

  case 21:
#line 159 "parser/parser.ypp"
    { (yyval.param) = read_integer_param((yyvsp[(1) - (1)].number)); }
    break;

  case 22:
#line 160 "parser/parser.ypp"
    { (yyval.param) = reinterpret_cast< unsigned long >((yyvsp[(1) - (1)].ident)); }
    break;

  case 23:
#line 163 "parser/parser.ypp"
    { (yyval.params) = new function_params(1, (yyvsp[(1) - (1)].param)); }
    break;

  case 24:
#line 165 "parser/parser.ypp"
    {
    function_params *p = (yyvsp[(1) - (3)].params);
    p->push_back((yyvsp[(3) - (3)].param));
    (yyval.params) = p;
  }
    break;

  case 25:
#line 172 "parser/parser.ypp"
    { (yyval.params) = NULL; }
    break;

  case 26:
#line 173 "parser/parser.ypp"
    { (yyval.params) = (yyvsp[(2) - (3)].params); }
    break;

  case 27:
#line 177 "parser/parser.ypp"
    {
    ast_ident *r = (yyvsp[(1) - (2)].ident);
    function_class const *f;
    if (function_params *p = (yyvsp[(2) - (2)].params)) {
      f = (*r->fun)(*p);
      delete p;
    } else f = (*r->fun)(function_params());
    if (!f) failure("invalid parameters for " << r->name);
    (yyval.function) = f;
  }
    break;

  case 29:
#line 191 "parser/parser.ypp"
    {
    function_class const *f = (yyvsp[(1) - (2)].function);
    if (f->type != UOP_ID) failure(f->name() << " is not a rounding operator");
    rounding_mode = f;
  }
    break;

  case 31:
#line 199 "parser/parser.ypp"
    {
    ast_ident *v = (yyvsp[(2) - (5)].ident);
    ast_real const *r = (yyvsp[(4) - (5)].real);
    if (v->type != ID_NONE) failure("the symbol " << (yyvsp[(2) - (5)].ident)->name << " is redefined");
    v->type = ID_VAR;
    v->var = r;
    if (r->name) warning(r->name->name << " is being renamed to " << v->name);
    r->name = v;
    rounding_mode = NULL;
  }
    break;

  case 32:
#line 210 "parser/parser.ypp"
    {
    ast_ident *v = (yyvsp[(3) - (6)].ident);
    v->type = ID_FUN;
    v->fun = new default_function_generator((yyvsp[(5) - (6)].function));
  }
    break;

  case 33:
#line 215 "parser/parser.ypp"
    { failure("the symbol " << (yyvsp[(2) - (2)].ident)->name << " is redefined"); }
    break;

  case 34:
#line 216 "parser/parser.ypp"
    { failure("the symbol " << (yyvsp[(2) - (2)].ident)->name << " is redefined"); }
    break;

  case 35:
#line 217 "parser/parser.ypp"
    { failure("the symbol " << (yyvsp[(3) - (3)].ident)->name << " is redefined"); }
    break;

  case 36:
#line 218 "parser/parser.ypp"
    { failure("the symbol " << (yyvsp[(3) - (3)].ident)->name << " is redefined"); }
    break;

  case 37:
#line 221 "parser/parser.ypp"
    { (yyval.real) = normalize(ast_real((yyvsp[(1) - (1)].number))); }
    break;

  case 38:
#line 222 "parser/parser.ypp"
    { (yyval.real) = (yyvsp[(1) - (1)].ident)->var; }
    break;

  case 39:
#line 224 "parser/parser.ypp"
    {
    ast_ident *v = (yyvsp[(1) - (1)].ident);
    v->type = ID_VAR;
    ast_real *r = normalize(ast_real(v));
    free_variables.insert(r);
    v->var = r;
    (yyval.real) = r;
  }
    break;

  case 40:
#line 233 "parser/parser.ypp"
    {
    function_class const *f = (yyvsp[(1) - (4)].function);
    ast_real_vect *ops = (yyvsp[(3) - (4)].reals);
    if ((f->type == UOP_ID && ops->size() != 1) ||
        (f->type != UOP_ID && f->type != COP_FMA && ops->size() != 2) ||
        (f->type == COP_FMA && ops->size() != 3))
      failure("incorrect number of arguments when calling " << f->name());
    (yyval.real) = normalize(ast_real(real_op(f, *ops)));
    delete ops;
  }
    break;

  case 41:
#line 243 "parser/parser.ypp"
    { (yyval.real) = rnd_normalize(ast_real(real_op((yyvsp[(1) - (3)].real), BOP_ADD, (yyvsp[(3) - (3)].real)))); }
    break;

  case 42:
#line 244 "parser/parser.ypp"
    { (yyval.real) = rnd_normalize(ast_real(real_op((yyvsp[(1) - (3)].real), BOP_SUB, (yyvsp[(3) - (3)].real)))); }
    break;

  case 43:
#line 245 "parser/parser.ypp"
    { (yyval.real) = rnd_normalize(ast_real(real_op((yyvsp[(1) - (3)].real), BOP_MUL, (yyvsp[(3) - (3)].real)))); }
    break;

  case 44:
#line 246 "parser/parser.ypp"
    { (yyval.real) = rnd_normalize(ast_real(real_op((yyvsp[(1) - (3)].real), BOP_DIV, (yyvsp[(3) - (3)].real)))); }
    break;

  case 45:
#line 247 "parser/parser.ypp"
    { (yyval.real) = normalize(ast_real(real_op(UOP_ABS, (yyvsp[(2) - (3)].real)))); }
    break;

  case 46:
#line 248 "parser/parser.ypp"
    { (yyval.real) = rnd_normalize(ast_real(real_op(UOP_SQRT, (yyvsp[(3) - (4)].real)))); }
    break;

  case 47:
#line 249 "parser/parser.ypp"
    { (yyval.real) = rnd_normalize(ast_real(real_op(UOP_LOG, (yyvsp[(3) - (4)].real)))); }
    break;

  case 48:
#line 250 "parser/parser.ypp"
    { (yyval.real) = rnd_normalize(ast_real(real_op(UOP_EXP, (yyvsp[(3) - (4)].real)))); }
    break;

  case 49:
#line 251 "parser/parser.ypp"
    { (yyval.real) = rnd_normalize(ast_real(real_op(UOP_LOG2, (yyvsp[(3) - (4)].real)))); }
    break;

  case 50:
#line 252 "parser/parser.ypp"
    { (yyval.real) = rnd_normalize(ast_real(real_op(UOP_EXP2, (yyvsp[(3) - (4)].real)))); }
    break;

  case 51:
#line 254 "parser/parser.ypp"
    {
    if (!parameter_rfma)
      (yyval.real) = rnd_normalize(ast_real(real_op(normalize(ast_real(real_op((yyvsp[(3) - (8)].real), BOP_MUL, (yyvsp[(5) - (8)].real)))), BOP_ADD, (yyvsp[(7) - (8)].real))));
    else
      (yyval.real) = rnd_normalize(ast_real(real_op((yyvsp[(7) - (8)].real), BOP_ADD, normalize(ast_real(real_op((yyvsp[(3) - (8)].real), BOP_MUL, (yyvsp[(5) - (8)].real)))))));
  }
    break;

  case 52:
#line 260 "parser/parser.ypp"
    { (yyval.real) = (yyvsp[(2) - (3)].real); }
    break;

  case 53:
#line 261 "parser/parser.ypp"
    { (yyval.real) = (yyvsp[(2) - (2)].real); }
    break;

  case 54:
#line 262 "parser/parser.ypp"
    { (yyval.real) = normalize(ast_real(real_op(UOP_NEG, (yyvsp[(2) - (2)].real)))); }
    break;

  case 55:
#line 265 "parser/parser.ypp"
    { (yyval.reals) = new ast_real_vect(1, (yyvsp[(1) - (1)].real)); }
    break;

  case 56:
#line 267 "parser/parser.ypp"
    {
    (yyvsp[(1) - (3)].reals)->push_back((yyvsp[(3) - (3)].real));
    (yyval.reals) = (yyvsp[(1) - (3)].reals);
  }
    break;

  case 57:
#line 273 "parser/parser.ypp"
    { (yyval.param) = fill_splitter(0, (yyvsp[(1) - (1)].number)); }
    break;

  case 58:
#line 274 "parser/parser.ypp"
    { (yyval.param) = fill_splitter((yyvsp[(1) - (3)].param), (yyvsp[(3) - (3)].number)); }
    break;

  case 59:
#line 277 "parser/parser.ypp"
    { dichotomy_var v = { (yyvsp[(1) - (1)].real), 0 }; (yyval.dvar) = v; }
    break;

  case 60:
#line 278 "parser/parser.ypp"
    { dichotomy_var v = { (yyvsp[(1) - (3)].real), read_integer_param((yyvsp[(3) - (3)].number)) }; (yyval.dvar) = v; }
    break;

  case 61:
#line 279 "parser/parser.ypp"
    { dichotomy_var v = { (yyvsp[(1) - (5)].real), (yyvsp[(4) - (5)].param) }; (yyval.dvar) = v; }
    break;

  case 62:
#line 282 "parser/parser.ypp"
    { (yyval.dvars) = new dvar_vect(1, (yyvsp[(1) - (1)].dvar)); }
    break;

  case 63:
#line 284 "parser/parser.ypp"
    {
    (yyvsp[(1) - (3)].dvars)->push_back((yyvsp[(3) - (3)].dvar));
    (yyval.dvars) = (yyvsp[(1) - (3)].dvars);
  }
    break;

  case 64:
#line 290 "parser/parser.ypp"
    { (yyval.precond) = new hint_cond(COND_NE, (yyvsp[(1) - (3)].real), (yyvsp[(3) - (3)].number)); }
    break;

  case 65:
#line 291 "parser/parser.ypp"
    { (yyval.precond) = new hint_cond(COND_LE, (yyvsp[(1) - (3)].real), (yyvsp[(3) - (3)].number)); }
    break;

  case 66:
#line 292 "parser/parser.ypp"
    { (yyval.precond) = new hint_cond(COND_GE, (yyvsp[(1) - (3)].real), (yyvsp[(3) - (3)].number)); }
    break;

  case 67:
#line 293 "parser/parser.ypp"
    { (yyval.precond) = new hint_cond(COND_LT, (yyvsp[(1) - (3)].real), (yyvsp[(3) - (3)].number)); }
    break;

  case 68:
#line 294 "parser/parser.ypp"
    { (yyval.precond) = new hint_cond(COND_GT, (yyvsp[(1) - (3)].real), (yyvsp[(3) - (3)].number)); }
    break;

  case 69:
#line 297 "parser/parser.ypp"
    { (yyval.preconds) = new hint_cond_vect(1, (yyvsp[(1) - (1)].precond)); }
    break;

  case 70:
#line 299 "parser/parser.ypp"
    {
    (yyvsp[(1) - (3)].preconds)->push_back((yyvsp[(3) - (3)].precond));
    (yyval.preconds) = (yyvsp[(1) - (3)].preconds);
  }
    break;

  case 71:
#line 305 "parser/parser.ypp"
    { (yyval.preconds) = NULL; }
    break;

  case 72:
#line 306 "parser/parser.ypp"
    { (yyval.preconds) = (yyvsp[(2) - (3)].preconds); }
    break;

  case 74:
#line 310 "parser/parser.ypp"
    {
    ast_real const *r1 = (yyvsp[(2) - (6)].real), *r2 = (yyvsp[(4) - (6)].real);
    hint_cond_vect *hc = (yyvsp[(5) - (6)].preconds);
    test_ringularity(r1, r2, !hc);
    std::string info;
    int t = test_rewriting(r1, r2, info);
    if (t == 1) {
      warning("no need for the rewriting");
      std::cerr << "  if one of the following relations is present\n" << info;
    } else if (t == 2) {
      warning("no need for the rewriting");
      if (!info.empty())
        std::cerr << "  if all the following properties can be proved\n" << info;
    }
    check_approx(r1);
    register_user_rewrite(r1, r2, hc);
    delete hc;
  }
    break;

  case 75:
#line 329 "parser/parser.ypp"
    {
    dichotomy_hint h;
    h.dst = *(yyvsp[(2) - (5)].reals); delete (yyvsp[(2) - (5)].reals);
    h.src = *(yyvsp[(4) - (5)].dvars); delete (yyvsp[(4) - (5)].dvars);
    dichotomies.push_back(h);
  }
    break;

  case 76:
#line 336 "parser/parser.ypp"
    {
    dichotomy_hint h;
    h.src = *(yyvsp[(3) - (4)].dvars); delete (yyvsp[(3) - (4)].dvars);
    dichotomies.push_back(h);
  }
    break;

  case 77:
#line 342 "parser/parser.ypp"
    {
    if (accurates[(yyvsp[(2) - (5)].real)].insert((yyvsp[(4) - (5)].real)).second)
      approximates[(yyvsp[(4) - (5)].real)].insert((yyvsp[(2) - (5)].real));
    else
      warning("relation already known");
  }
    break;

  case 78:
#line 349 "parser/parser.ypp"
    {
    ast_real const *r = (yyvsp[(2) - (5)].real);
    ast_ident *t = (yyvsp[(4) - (5)].ident);
    if (register_user_external(r,t) != 0) {
      failure(t->name << " is an unknown external tool");
    }
  }
    break;


/* Line 1267 of yacc.c.  */
#line 2113 "parser/parser.cpp"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }

  yyerror_range[0] = yylloc;

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval, &yylloc);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  yyerror_range[0] = yylsp[1-yylen];
  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;

      yyerror_range[0] = *yylsp;
      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp, yylsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  *++yyvsp = yylval;

  yyerror_range[1] = yylloc;
  /* Using YYLLOC is tempting, but would change the location of
     the look-ahead.  YYLOC is available though.  */
  YYLLOC_DEFAULT (yyloc, (yyerror_range - 1), 2);
  *++yylsp = yyloc;

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEOF && yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval, &yylloc);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp, yylsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}



