#ifndef GAPPA_EXTERNAL_AAMPFR_H
#define GAPPA_EXTERNAL_AAMPFR_H

#include <vector>
#include <stdio.h>
#include <mpfr.h>

namespace AA {

  class AAM {
  public:
    typedef unsigned int index_t;

  private:
    static index_t LastIdx;  // Highest noise symbol in use

    int Prec;

    typedef std::pair<index_t, __mpfr_struct> coeff_t; 
    typedef std::vector<coeff_t>              coeffstore_t;
    typedef coeffstore_t::iterator            coeff_iterator;
    typedef coeffstore_t::const_iterator      coeff_const_iterator;

    mpfr_t       Center;
    coeffstore_t Coeffs;

    AAM();
    
  public:
    explicit AAM(int prec);
    AAM(const AAM &aam);
    AAM(const mpfr_t &center, int prec);
    AAM(const mpfr_t &lower, const mpfr_t &upper, int prec);
    AAM(const AAM &x, const mpfr_t &alpha, const mpfr_t &beta, const mpfr_t &gamma);
    ~AAM();

    AAM  operator - () const;

    AAM& operator = (const AAM &rhs);
    AAM  operator + (const AAM &rhs) const;
    AAM  operator - (const AAM &rhs) const;
    AAM  operator * (const AAM &rhs) const;
    AAM  operator / (const AAM &rhs) const;
 
    friend AAM inv(const AAM &x); 
    friend AAM exp(const AAM &x);
    friend AAM log(const AAM &x);
    friend AAM exp2(const AAM &x);
    friend AAM log2(const AAM &x);
    friend AAM sqrt(const AAM &x);

    void convert(mpfr_t &lower, mpfr_t &upper) const;

    coeffstore_t::size_type numCoeffs() const { return Coeffs.size(); }
    void dump(FILE *fh) const;

  private:
    static index_t incLast() { return LastIdx++; }
    static void mid_rad(mpfr_t mid, mpfr_t rad, mpfr_t lo, mpfr_t hi, int prec);

    void radius(mpfr_t rad) const;
    
    coeff_iterator coeff_begin() { return Coeffs.begin(); }
    coeff_iterator coeff_end() { return Coeffs.end(); }

    coeff_const_iterator coeff_begin() const { return Coeffs.begin(); }
    coeff_const_iterator coeff_end() const { return Coeffs.end(); }
  
    static inline index_t  i2idx(const coeff_iterator& i) { return i->first; }
    static inline index_t  i2idx(const coeff_const_iterator& i) { return i->first; }

    static inline mpfr_ptr i2m(const coeff_iterator& i) { return &(i->second); }
    static inline const __mpfr_struct* i2m(const coeff_const_iterator& i) { return &(i->second); }
 
    static inline mpfr_ptr r2m(coeff_t& c) { return &(c.second); }
    static inline const __mpfr_struct* r2m(const coeff_t &c) { return &(c.second); }

    inline coeff_t& push_back(const coeff_const_iterator &i) { return push_back(i2idx(i),i2m(i)); }
    inline coeff_t& push_back(index_t idx) {
      mpfr_t tmp; mpfr_init2(tmp, Prec);
      Coeffs.push_back(std::make_pair(idx,*(mpfr_ptr)tmp));
      return Coeffs.back();
    }
    inline coeff_t& push_back(index_t idx, const __mpfr_struct *m) {
      mpfr_t tmp; mpfr_init2(tmp, Prec);
      mpfr_set(tmp, m, GMP_RNDN);
      Coeffs.push_back(std::make_pair(idx,*(mpfr_ptr)tmp));
      return Coeffs.back();
    }

    void reserve(coeffstore_t::size_type size) { Coeffs.reserve(size); }
  };


  AAM inv(const AAM &x);
  AAM exp(const AAM &x);
  AAM log(const AAM &x);
  AAM exp2(const AAM &x);
  AAM log2(const AAM &x);
  AAM sqrt(const AAM &x);

} // end of namespace AA

#endif
