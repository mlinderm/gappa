#include "hr_AST.hpp"
using namespace EXT;
using namespace HR;

Expr::Idx_t Expr::CurEdgeIdx_d = 0;


int RoundingExpr::getPrec() const {
  switch(getMode()) {
  case CUDA_32:  return 24;
  case IEEE_32:  return 24;
  case IEEE_64:  return 53;
  case x87_80:   return 113;
  case IEEE_128: return 113;
  case FIXED:    return Prec_d;
  }  
}

bool RoundingExpr::operator>=(const RoundingExpr& rhs) const {
  if (getMode() != rhs.getMode() || getDirection() != getDirection()) return false;
  else if (getMode() == FIXED &&
	   getPrec() <= rhs.getPrec()) return true;
  else if (getMode() != FIXED && (getMode() <= rhs.getMode())) return true;
  else return false;
}

Expr ExprBuilder::constantExpr(const IV& iv) {
  return Expr(new ConstantExpr(iv));
}

Expr ExprBuilder::constantExpr(int i) {
  return Expr(new ConstantExpr(IV(Num(i),Num(i))));
}

Expr ExprBuilder::identExpr(const std::string& name, const IV& iv) {
  return Expr(new IdentExpr(name, iv));
}

Expr ExprBuilder::roundExpr(RoundingExpr::Mode mode, RoundingExpr::Direction dir, 
			    const IV& err, const Expr& arg) {
  return Expr(new RoundingExpr(mode, dir, err, arg));
}

Expr ExprBuilder::roundExpr(RoundingExpr::Mode mode, RoundingExpr::Direction dir, int prec, 
			    const IV& err, const Expr& arg) {
  return Expr(new RoundingExpr(mode, dir, prec, err, arg));
}

#define UNARY_BUILD( NAME, OP ) \
  Expr ExprBuilder::NAME  ## Expr(const Expr& arg) { return Expr(new UnaryExpr(UnaryExpr::OP, arg)); }

UNARY_BUILD(log, Log);
UNARY_BUILD(log2, Log2);
UNARY_BUILD(exp, Exp);
UNARY_BUILD(exp2, Exp2);
UNARY_BUILD(logp1, Logp1);
UNARY_BUILD(log2p1, Log2p1);

#undef UNARY_BUILD


#define BINARY_BUILD( NAME, OP ) \
  Expr ExprBuilder::NAME ## Expr(const Expr& lhs, const Expr& rhs) {	\
    return Expr(new BinaryExpr(BinaryExpr::OP, lhs, rhs));		\
  }

BINARY_BUILD(mul, Mul);
BINARY_BUILD(div, Div);
BINARY_BUILD(add, Add);
BINARY_BUILD(sub, Sub);

#undef BINARY_BUILD


Expr ExprBuilder::affineExpr(const IV& iv) {
  return Expr(new AffineExpr(iv));
}

Expr ExprBuilder::affineExpr(const AA::AAM& aa) {
  return Expr(new AffineExpr(aa));
}
