#include <iostream>
#include "ext_num.hpp"
using namespace EXT;

  
Num::NumVal *Num::NumVal::empty_NumVal() {
  static NumVal* empty_NumVal_g = new NumVal();
  mpfr_set_nan(empty_NumVal_g->Val_d);
  return empty_NumVal_g;
}

Num::NumVal *Num::NumVal::pos_inf_NumVal() {
  static NumVal* pos_inf_NumVal_g = new NumVal();
  mpfr_set_inf(pos_inf_NumVal_g->Val_d, 1);
  return pos_inf_NumVal_g;
}

Num::NumVal *Num::NumVal::neg_inf_NumVal() {
  static NumVal* neg_inf_NumVal_g = new NumVal();
  mpfr_set_inf(neg_inf_NumVal_g->Val_d, -1);
  return neg_inf_NumVal_g;
}


Num::Num() : Num_d(Num::nan().Num_d) {}

Num Num::nan() {
  static Num* nan_g = new Num(NumVal::empty_NumVal());
  return *nan_g;
}

Num Num::pos_inf() {
  static Num* pos_inf_g = new Num(NumVal::pos_inf_NumVal());
  return *pos_inf_g;
}

Num Num::neg_inf() {
  static Num* neg_inf_g = new Num(NumVal::neg_inf_NumVal());
  return *neg_inf_g;
}


Num EXT::opp(const Num &x, mp_rnd_t rnd) {
  Num n(new Num::NumVal());
  mpfr_neg(n.Num_d->Val_d,x.Num_d->Val_d,rnd);
  return n;
}

Num EXT::sqrt(const Num &x, mp_rnd_t rnd) {
  Num n(new Num::NumVal());
  mpfr_sqrt(n.Num_d->Val_d,x.Num_d->Val_d,rnd);
  return n;
}

Num EXT::exp(const Num &x, mp_rnd_t rnd) {
  Num n(new Num::NumVal());
  mpfr_exp(n.Num_d->Val_d,x.Num_d->Val_d,rnd);
  return n;
}

Num EXT::log(const Num &x, mp_rnd_t rnd) {
  Num n(new Num::NumVal());
  mpfr_log(n.Num_d->Val_d,x.Num_d->Val_d,rnd);
  return n;
}

Num EXT::exp2(const Num &x, mp_rnd_t rnd) {
  Num n(new Num::NumVal());
  mpfr_exp2(n.Num_d->Val_d,x.Num_d->Val_d,rnd);
  return n;
}

Num EXT::log2(const Num &x, mp_rnd_t rnd) {
  Num n(new Num::NumVal());
  mpfr_log2(n.Num_d->Val_d,x.Num_d->Val_d,rnd);
  return n;
}

Num EXT::add(const Num &lhs, const Num &rhs, mp_rnd_t rnd) {
  Num n(new Num::NumVal());
  mpfr_add(Num::n2mpfr(n),Num::n2mpfr(lhs),Num::n2mpfr(rhs),rnd);
  return n;
}

Num EXT::sub(const Num &lhs, const Num &rhs, mp_rnd_t rnd)  {
  Num n(new Num::NumVal());
  mpfr_sub(Num::n2mpfr(n),Num::n2mpfr(lhs),Num::n2mpfr(rhs),rnd);
  return n;
}

Num EXT::mul(const Num &lhs, const Num &rhs, mp_rnd_t rnd)  {
  Num n(new Num::NumVal());
  mpfr_mul(Num::n2mpfr(n),Num::n2mpfr(lhs),Num::n2mpfr(rhs),rnd);
  return n;
}

Num EXT::div(const Num &lhs, const Num &rhs, mp_rnd_t rnd) {
  Num n(new Num::NumVal());
  mpfr_div(Num::n2mpfr(n),Num::n2mpfr(lhs),Num::n2mpfr(rhs),rnd);
  return n;
}

Num EXT::median(const Num &lo, const Num &hi) {
  Num n(new Num::NumVal());
  mpfr_add(Num::n2mpfr(n),Num::n2mpfr(lo),Num::n2mpfr(hi),GMP_RNDN);
  mpfr_div_2ui(Num::n2mpfr(n), Num::n2mpfr(n), 1, GMP_RNDN);
  return n;
}

std::ostream& EXT::operator<<(std::ostream& os, const Num& num) {
  os << mpfr_get_d(num.Num_d->Val_d,GMP_RNDN);
  return os;
}
  
