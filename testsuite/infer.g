# Test case for exp/log functions

ls  = fixed<-24,dn>(ls_m);
acc = fixed<-24,dn>(acc_m);

x_m = ls_m-acc_m;
x fixed<-24,dn> = ls - acc;

f_m = log(1+exp(x_m));
f_  = log(1+exp(fixed<-4,dn>(x)));
f   = fixed<-24,dn>(f_);

acc_n_m = acc_m + f_m;
acc_n   = acc + f;

{ls_m in [-1000,0] /\ acc_m in [-1000,0] -> f-f_m in ? /\ acc_n-acc_n_m in ?}



