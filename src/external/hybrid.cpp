#include <cassert>
#include <set>
#include <map>
#include <iostream>

#include <boost/bind.hpp>

#include "numbers/interval.hpp"
#include "numbers/interval_utility.hpp"
#include "numbers/real.hpp"
#include "parser/pattern.hpp"
#include "parser/ast.hpp"

#include "external.hpp"
#include "hr_AST.hpp"
#include "hr_AST_utils.hpp"
#include "hr_AST_rewriting.hpp"
using namespace EXT;
using namespace HR;

class HybridExt : public External {
private:
  
  typedef std::set<predicated_real>   Needed_set_t;
  typedef std::set<const ast_real *>  gAST_set_t;

  typedef std::map< ast_real const *, ast_real const*>  gAST2gAST_map_t;
  typedef std::map< ast_real const *, Expr>             gAST2hAST_map_t;
  typedef std::map< ast_real const *, interval >        gAST2gIV_map_t;

  gAST2gIV_map_t      AST2IV_d;   // Map gappa AST nodes to enclosures
  gAST2gAST_map_t     Rnd2Err_d;  // Map gappa AST round ops to error expression
  gAST2hAST_map_t     AST2hAST_d; // Map gappa AST nodes to hybrid nodes

public:
  // External API

  interval compute(const ast_real *target, const std::vector<property> &hyps);

  bool canCreateScheme(predicated_real const &target);

  void fillNeededReals(preal_vect &needed, const ast_real *target);
 
private:

  bool canCreateScheme(const ast_real *r);

  void fillNeededReals(Needed_set_t &needed, gAST_set_t &visited, const ast_real *r);

  Expr gappa2HRAST(const ast_real *r);
  Expr gappa2HRAST(function_class const *f, ast_real const *r);

  inline const Expr& cacheHRAST(const ast_real *r, const Expr &expr) {
    return AST2hAST_d.insert(std::make_pair(r, expr)).first->second;
  }
};

// Create ExternalStar factory for hybrid IA-AlgebraicRewriting/Affine
// error estimation

namespace {

  HybridExt he;

  class Hybrid_Factory {
  public:
    Hybrid_Factory(HybridExt &tool, predicated_real const &p) {
      new ExternalSelect_Factory(tool,p);
    }
  };

} // end of anonymous namespace


// Interface for user instantiation of the hybrid tool, invoked
// by the parser, return 0 to indicate success
int 
register_user_hybrid_tool(ast_real const *r) {
  new Hybrid_Factory(he,predicated_real(r, PRED_BND));
  return 0;
}


// ---------------------------------------------------
// 
// Implementation of Hybrid Tool
//
// ---------------------------------------------------

bool 
HybridExt::canCreateScheme(const ast_real *r) {
  if (real_op const *p = boost::get< real_op const >(r)) {
    //fprintf(stderr,"Real Op: %s Type: %d\n",dump_real(r).c_str(),p->type);
    switch (p->type) {
    default:
      fprintf(stderr,"Dump w/ Op: %s Type: %d\n",dump_real(r).c_str(),p->type);
      assert(false && "Function not yet supported");
      break;
    case UOP_NEG:
    case UOP_EXP:
    case UOP_LOG:
    case UOP_EXP2:
    case UOP_LOG2:
      return canCreateScheme(p->ops[0]);
    case BOP_ADD:
    case BOP_SUB:
      return canCreateScheme(p->ops[0]) && canCreateScheme(p->ops[1]);
    case ROP_FUN:
      return canCreateScheme(p->ops[0]);
    }
  } else if (hidden_real const *h = boost::get< hidden_real const >(r)) {
    return canCreateScheme(h->real);
  } else if (ast_number const * const *n = boost::get< ast_number const *>(r)) {
    //fprintf(stderr,"Number: %s\n",dump_real(r).c_str());
    return true;
  } else if (undefined_real const *u = boost::get< undefined_real const >(r)) {
    //fprintf(stderr,"Identifier: %s\n",dump_real(r).c_str());
    return true;
  } else {
    assert(false && "Kind of AST real not yet supported");
  }
}

bool
HybridExt::canCreateScheme(predicated_real const &target) {
  return canCreateScheme(target.real());
}

  
void
HybridExt::fillNeededReals(Needed_set_t &needed, gAST_set_t &visited, const ast_real *r) {
  if (visited.find(r) != visited.end())
    return;

  if (real_op const *p = boost::get< real_op const >(r)) {
    switch (p->type) {
    default: break;
    case ROP_FUN: {
      // For rnd(x), we include BND(rnd(x)-x) as a needed real to compute
      // the error using Gappa's full capabilities
      ast_real const *ex = p->ops[0];
      ast_real const *ee = normalize(ast_real(real_op(r, BOP_SUB, ex)));
      Rnd2Err_d[r] = ee;
      needed.insert(predicated_real(ee, PRED_BND));
    }
    }

    typedef ast_real_vect::const_iterator iter_t;
    for(iter_t i = p->ops.begin(), end = p->ops.end(); i != end; ++i) {
      fillNeededReals(needed, visited, *i);
    }
  } else if (hidden_real const *h = boost::get< hidden_real const >(r)) {
    fillNeededReals(needed, visited, h->real);
  } else {
    // Base case for recursion - an identifier or number
    needed.insert(predicated_real(r, PRED_BND));
  }

  visited.insert(r);
}

void 
HybridExt::fillNeededReals(preal_vect &needed, const ast_real *target) {
  Needed_set_t needed_set;
  gAST_set_t   visited_set;

  fillNeededReals(needed_set, visited_set, target);
  
  needed.insert(needed.end(),needed_set.begin(),needed_set.end());
}

// Gappa functions defined elsewhere
interval create_interval(ast_number const *lower, ast_number const *upper, bool widen);

namespace {
  
  RoundingExpr::Direction get_direction(const std::string& dir) {
    if      (dir.compare(0,2,"ne") == 0) return RoundingExpr::ROUND_NE;
    else if (dir.compare(0,2,"dn") == 0) return RoundingExpr::ROUND_DN;
    else if (dir.compare(0,2,"up") == 0) return RoundingExpr::ROUND_UP;
    else if (dir.compare(0,2,"zr") == 0) return RoundingExpr::ROUND_ZR;
    else {
      fprintf(stderr,"Rounding Direction: %s\n",dir.c_str());
      assert(false && "Rounding direction not yet supported");
      return RoundingExpr::ROUND_NE;
    }
  }
  
  RoundingExpr::Mode get_float_mode(const std::string& rnd_lim) {
    switch(rnd_lim.length()) {
    default: break;
    case 6:
      if (rnd_lim.compare(0,6,"24,149") == 0) return RoundingExpr::IEEE_32;
      else break;
    case 7:
       if (rnd_lim.compare(0,7,"53,1074") == 0) return RoundingExpr::IEEE_64;
       else break;
    }

    fprintf(stderr,"Rounding mode: %s\n",rnd_lim.c_str());
    assert(false && "Float rounding mode not yet supported");
    return RoundingExpr::IEEE_32;
  }

  IV gIV2hIV(interval& i) {
    return IV(Num(lower(i).mpfr_data()), Num(upper(i).mpfr_data()));
  }

  interval hIV2gIV(const IV& iv) {
    number_base *lo = new number_base, *hi = new number_base;
    iv2mpfr((mpfr_t&)(lo->val), (mpfr_t&)(hi->val), iv);
    return interval(number(lo),number(hi));
  }

  IV hAST2hIV(const Expr& expr) { return Expr2IV().Visit(expr); }

  interval hAST2gIV(const Expr& expr) {
    number_base *lo = new number_base, *hi = new number_base;
    IV iv = hAST2hIV(expr);
    iv2mpfr((mpfr_t&)(lo->val), (mpfr_t&)(hi->val), iv);
    return interval(number(lo),number(hi));
  }

  
} // end of anonymous namespace

Expr
HybridExt::gappa2HRAST(function_class const *f, ast_real const *r) {
  // Obtain Gappa estimate of the error
  gAST2gAST_map_t::iterator ee_iter = Rnd2Err_d.find(r); assert(ee_iter != Rnd2Err_d.end());
  gAST2gIV_map_t::iterator  iv_iter = AST2IV_d.find(ee_iter->second); assert(iv_iter != AST2IV_d.end());
  
  real_op const *op = boost::get< real_op const >(r); assert(op);
  
  Expr expr = gappa2HRAST(op->ops[0]);

  // Take the lesser of the rounding errors that Gappa estimates and the
  // error estimated from the affine bound
  std::string tmp;
  interval 
    h_errest = f->absolute_error_from_exact_bnd(hAST2gIV(expr),tmp),
    err = (iv_iter->second < h_errest) ? iv_iter->second : h_errest;

  // Parse out rounding mode from Gappa representation
  std::string name = f->name();
  if (name.compare(0, 14, "rounding_float") == 0) {
    return ExprBuilder::roundExpr(get_float_mode(name.substr(18)),
				  get_direction(name.substr(15,2)),
				  gIV2hIV(err),
				  expr);
  } else {
    fprintf(stderr,"Rounding kind %s\n",f->name().c_str()); 
    assert(false && "Unknown rounding mode"); 
    return Expr();
  }
}


Expr 
HybridExt::gappa2HRAST(const ast_real *r) {

  { // Look for previously computed AST nodes
    gAST2hAST_map_t::iterator i = AST2hAST_d.find(r);
    if (i != AST2hAST_d.end())
      return i->second;
  }

  if (real_op const *p = boost::get< real_op const >(r)) {
 
    switch (p->type) {
    case UOP_EXP:  return cacheHRAST(r, ExprBuilder::expExpr(gappa2HRAST(p->ops[0])));
    case UOP_LOG:  return cacheHRAST(r, ExprBuilder::logExpr(gappa2HRAST(p->ops[0])));
    case UOP_EXP2: return cacheHRAST(r, ExprBuilder::exp2Expr(gappa2HRAST(p->ops[0])));
    case UOP_LOG2: return cacheHRAST(r, ExprBuilder::log2Expr(gappa2HRAST(p->ops[0])));
    case BOP_ADD:  return cacheHRAST(r, ExprBuilder::addExpr(gappa2HRAST(p->ops[0]),gappa2HRAST(p->ops[1])));
    case BOP_SUB:  return cacheHRAST(r, ExprBuilder::subExpr(gappa2HRAST(p->ops[0]),gappa2HRAST(p->ops[1])));
    case ROP_FUN:  return cacheHRAST(r, gappa2HRAST(p->fun, r));
    default:
      fprintf(stderr,"Real Op: %s Type: %d\n",dump_real(r).c_str(),p->type);
      assert(false && "Function not yet supported");
      break;
    }
  
  } else if (hidden_real const *h = boost::get< hidden_real const >(r)) {
  
    return gappa2HRAST(h->real);
  
  } else if (ast_number const * const *n = boost::get< ast_number const *>(r)) {

    gAST2gIV_map_t::iterator it = AST2IV_d.find(r); assert(it != AST2IV_d.end());
    return cacheHRAST(r, ExprBuilder::constantExpr(gIV2hIV(it->second)));

  } else if (undefined_real const *u = boost::get< undefined_real const >(r)) {

    const ast_ident *id = r->name;  assert(id && id->type == ID_VAR);
    gAST2gIV_map_t::iterator it = AST2IV_d.find(r); assert(it != AST2IV_d.end());
    return cacheHRAST(r, ExprBuilder::identExpr(id->name, gIV2hIV(it->second)));

  } 

  assert(false); // Should not reach here
  return Expr();
}


/* -----------------------------------------------------
 * 
 * Rewriting Visitors
 *
 * -----------------------------------------------------*/

namespace {

  typedef ExprBuilder EB;

  // Bypass "zero error" rouding operations, convert others to affine expressions
  class Round : public RW::RewriteRule {    
  private:
    // Convert to affine
    struct LS {
      RoundingExpr::Mode      Mode_d;
      RoundingExpr::Direction Dir_d;
      LS(const RoundingExpr *rnd) : Mode_d(rnd->getMode()), Dir_d(rnd->getDirection()) {}
      bool operator()(const RW::match_result& mr) const { 
	const RoundingExpr* re = mr.get<RoundingExpr>(0);
	return (Mode_d != RoundingExpr::FIXED && 
		(Dir_d == re->getDirection() && Mode_d < re->getMode()));
      }
    };
    LS LS_d;

    static Expr rnd2aa(const RW::match_result& mr) {
      const RoundingExpr* re = mr.get<RoundingExpr>(0);
      return EB::addExpr(re->getOperand(), EB::affineExpr(re->getErr()));
    }

    // Bypass
    static Expr bypass(const RW::match_result& mr) { 
      return mr.get<RoundingExpr>(0)->getOperand(); 
    }
    struct Zero {
      bool operator()(const RW::match_result& mr) const { 
	return is_zero(mr.get<RoundingExpr>(0)->getErr()); 
      }
    };
    Zero Zero_d;

    RW::rule<> r;

  public:
 
    Round(const RoundingExpr *rnd) : LS_d(rnd) {
      r =   (RW::R::rnd0 >>= Zero_d) >> &bypass
	  | (RW::R::rnd0 >>= LS_d)   >> &rnd2aa;
    }
  
    RW::rule<>& start() { return r; }
  };
  
  // Swap operands in binary expr to LHS
  struct Swap_Operands : RW::RewriteRule {
    static Expr swap(const RW::match_result& mr) {
       const BinaryExpr* be = mr.get<BinaryExpr>(0);
       return EB::addExpr(be->getRHS(), be->getLHS());
    }
  
    struct Sub {
      bool operator()(const RW::match_result& mr) {
      const BinaryExpr* be = mr.get<BinaryExpr>(0);
      // Is LHS < RHS or a subset of RHS ?
      IV lhs = hAST2hIV(be->getLHS()), rhs =  hAST2hIV(be->getRHS());
      return (subset(lhs,rhs) || (!overlap(lhs, rhs) && lhs < rhs));
      }
    } Sub_d;
  
    RW::rule<> r;

    Swap_Operands() {
      r = (RW::R::bin0 >>= Sub_d) >> &swap;
    }

    RW::rule<>& start() { return r; }
  };
  
  // Combine affine expressions
  struct Combine_Affine : RW::RewriteRule {
    
    static Expr aff(const RW::match_result& mr) {
      return EB::addExpr(mr.get<BinaryExpr>(1)->getLHS(),
			 EB::affineExpr(mr.get<AffineExpr>(2)->getVal() +
					mr.get<AffineExpr>(3)->getVal()));
    }
     
    static Expr cnst(const RW::match_result& mr) {
      return EB::addExpr(mr.get<BinaryExpr>(1)->getLHS(),
			 EB::affineExpr(mr.get<AffineExpr>(3)->getVal() + 
					mr.get<ConstantExpr>(2)->getVal()));
    }
    
    RW::rule<> r;

    Combine_Affine() {
      using namespace RW;
      r =   R::add0[ R::add1 [ R::n , R::aff2 ] , R::aff3 ] >> &aff
	  | R::add0[ R::add1 [ R::n , R::cnst2 ] , R::aff3 ] >> &cnst;
    }

    RW::rule<>& start() { return r; }
  };
  
  // Expand transcendental functions
  struct Expand_Trans : RW::RewriteRule {
    static Expr log_expnd(const RW::match_result& mr) {
      const BinaryExpr* add  = mr.get<BinaryExpr>(0);
      return EB::addExpr(EB::logExpr(add->getLHS()),
			 EB::logp1Expr(EB::divExpr(add->getRHS(),
						   add->getLHS())));
    }
  
    struct Assym {
      bool operator()(const RW::match_result& mr) const {
	const BinaryExpr* add  = mr.get<BinaryExpr>(0);
	// Only want situations with assymmetric inputs
	IV lhs = hAST2hIV(add->getLHS()), rhs =  hAST2hIV(add->getRHS());
	return (!overlap(lhs,rhs) && lhs > rhs);
      }
    } Assym_d;

    RW::rule<> r;
    RW::rule<>& start() { return r; }

    Expand_Trans() {
      using namespace RW;
      r = (R::log[ R::add0 [ R::n , R::n ] ] >>= Assym_d) >> &log_expnd;
    }
  };
  
  // Contract transcendental functions
  struct Contract_Trans : RW::RewriteRule {
    static Expr simplify(const RW::match_result& mr) {
      return mr.get<UnaryExpr>(0)->getOperand();
    }
  
    RW::rule<> r;
    RW::rule<>& start() { return r; }
    
    Contract_Trans() {
      using namespace RW;
      r = R::log[ R::exp0 [ R::n ] ] >> &simplify;
    }
  };

} // end of anonymous namespace


namespace {
  class Compute_Error : public ExprVisitor<Compute_Error,IV> {
  private:
    
    const RoundingExpr *Re_d;
    
    bool influence(const IV& victim, const IV& agent, mp_rnd_t rnd, int prec) {
      if (in_zero(victim))
	return true;

      // Most challenging rounding case
      Num      vic     = (victim.lower() > 0) ? victim.lower() : opp(victim.upper());
      mp_exp_t err_exp = 
	mpfr_get_exp(vic.getVal()) - mpfr_get_exp(norm(agent).getVal());
      return (err_exp <= ((rnd == GMP_RNDN) ? prec+1 : prec));
    }


  public:
    Compute_Error(const RoundingExpr *re) : Re_d(re) { assert(re->getMode() != RoundingExpr::FIXED); }

    IV Visit(Expr& expr) { return ExprVisitor<Compute_Error,IV>::Visit(expr.newest()); }
    IV Visit(const Expr& expr) { return ExprVisitor<Compute_Error,IV>::Visit((ExprValue*)expr.newest()); }
    
    IV VisitBinaryExpr(BinaryExpr *expr) {
      switch (expr->getOpcode()) {
      default: assert(false); break;
      case BinaryExpr::Add: 
      case BinaryExpr::Sub: {
	if (!influence(hAST2hIV(expr->getLHS()), hAST2hIV(expr->getRHS()),
		       GMP_RNDN, Re_d->getPrec()))
	  return Visit(expr->getLHS()) + Visit(expr->getRHS());
      }
      }
      return VisitExpr(expr);
    }
    
    IV VisitRoundingExpr(RoundingExpr *expr) {
      if (*expr >= *Re_d) return IV(0, 0);
      else                return expr->getErr();
    }

    IV VisitExpr(ExprValue *expr) { return Expr2IV().ExprVisitor<Expr2IV,IV>::Visit(expr); }
  };


} // end of anonymous namespace

interval 
HybridExt::compute(const ast_real *target, const std::vector<property> &hyps) {
  
  // Update the enclosure map
  {
    AST2IV_d.clear();
    AST2hAST_d.clear();

    typedef std::vector<property>::const_iterator iter_t;
    for (iter_t i=hyps.begin(), e=hyps.end(); i!=e; ++i) {
      AST2IV_d[i->real.real()] = i->bnd();
    }
  }

  // Generate the hybrid AST
  Expr ast = gappa2HRAST(target);
  
  // What are we trying to accomplish?
  using namespace RW;
  
  // 1. Improved error estimation -- MATCH(Round(x)-x)

  rule<> rnd_err = R::sub [ R::rnd0 [ R::n1 ], R::n1 ];
  if (match_result mr = rnd_err.match(&ast,&ast+1)) {
    const RoundingExpr* re = ExprValue::dyn_cast<RoundingExpr>(mr[0]); assert(re);

    Round rr(re);
    ExprRewriter::expr_rewrite(rr, (Expr&)(re->getOperand()));
 
    Swap_Operands sw;
    ExprRewriter::expr_rewrite(sw, (Expr&)(re->getOperand()));
    
    Combine_Affine ca;
    ExprRewriter::expr_rewrite(ca, (Expr&)(re->getOperand()));
    ExprRewriter::expr_rewrite(ca, (Expr&)(re->getOperand()));
    
    Expand_Trans et;
    ExprRewriter::expr_rewrite(et, (Expr&)(re->getOperand()));
    
    Contract_Trans ct;
    ExprRewriter::expr_rewrite(ct, (Expr&)(re->getOperand()));
    
    //ExprPrinter(std::cerr).Visit((Expr&)(re->getOperand()));
    
    IV err_est = Compute_Error(re).Visit((Expr&)(re->getOperand()));
    
    if (subset(err_est,re->getErr())) {
      return hIV2gIV(err_est);
    } else {
      return hIV2gIV(re->getErr());
    }
  }

  return hAST2gIV(ast);
}
