<section>
<title>Rewriting</title>

<para>Internally, Gappa tries to compute the range of mathematical terms. For
example, if the tool has to bound the product of two factors, it will check if
it knows the ranges of both factors. If it does, it will apply the theorem
about real multiplication in order to compute the range of the product.</para>

<para>Unfortunately, there may be some expressions that Gappa cannot bound
correctly. This usually happens because it has no result on a sub-term or
because the expression is badly correlated. In this case, the user can provide
an alternate expression. If Gappa finds an enclosure for this secondary
expression, it will use its range in an enclosure for the primary expression.</para>

<programlisting><![CDATA[primary -> secondary;]]></programlisting>

<para>In order for this transformation to be valid, any evaluation of the
primary expression must be contained in the enclosure of the secondary
expression. Gappa will generate the corresponding theorem in the final proof
script and its proof will be left as an exercise for the user.</para>

<para>This transformation is generally valid if both expressions are equal.
To detect mistyping early, Gappa will try to check if they are indeed equal and
warn if they are not. It will not generate a proof of their equality though and
it is still up to the user to do it. Note that Gappa does not check if the
divisor are always different from zero in the expressions. Instead it warn about
such potentially bad divisors.</para>

<para>The internal database of rules check for null divisors though. As a
consequence, if a common rule does not apply, this may mean that such a divisor
exists. In order to test whether it is the case or not, the
<code>-Munconstrained</code> option allows to skip these checks. If Gappa is
happy when this option is provided, the input should be checked for null
divisors. Note that this option obviously prevents the proof generation.</para>
</section>

<section>
<title>Approximations</title>

<para>As mentioned before, Gappa has an internal database of rewriting rules.
Some of these rules need to know about accurate and approximate terms. Without
this information, they do not match any expression. For example, Gappa will
replace the term <code>B</code> by <code>b + -(b - B)</code> only it knows a
term <code>b</code> that is an approximation of the term <code>B</code>.</para>

<para>Gappa has two heuristics to detect terms that are approximations of other
terms. First, rounded values are approximations of their non-rounded
counterparts. Second, any absolute or relative error that appears as a
hypothesis of a logical sub-formula will define a pair of accurate and
approximate terms.</para>

<para>Since these pairs create lots of already proved rewriting rules, it is
helpful for the user to define its own pairs. This can be done with the
following syntax.</para>

<programlisting><![CDATA[approximate ~ accurate;]]></programlisting>

<para>In the following example, the comments show two hints that could be added
if they had not already been guessed by Gappa. In particular, the second one
enables a rewriting rule that completes the proof.</para>

<programlisting><![CDATA[@floor = int<dn>;
{ x - y in [-0.1,0.1] -> floor(x) - y in ? }
# floor(x) ~ x;
# x ~ y;]]></programlisting>
</section>

<section>
<title>Bisection</title>

<para>The last kind of hint can be used when Gappa is unable to prove a formula
but would be able to prove it if the hypothesis ranges were not so wide. Such a
failure is usually caused by a bad correlation between the sub-terms of the
expressions. This can be solved by rewriting the expressions. But the failure
can also happen when the proof of the formula is not the same everywhere on the
domain, as in the following example. In both cases, the user can ask Gappa to
split the ranges into tighter ranges and see if it helps proving the formula.</para>

<programlisting><![CDATA[@rnd = float< ieee_32, ne >;
x = rnd(x_);
y = x - 1;
z = x * (rnd(y) - y);
{ x in [0,3] -> |z| in ? }]]></programlisting>

<para>With this script, Gappa will answer that <texinline>|z| \le 3 \cdot
2^{-24}</texinline>. This is not the best answer because Gappa does not notice
that <code>rnd(y) - y</code> is always zero when <texinline>\frac{1}{2} \le x
\le 3</texinline>. The user needs to ask for a bisection with respect to the
expression <code>rnd(x_)</code>.</para>

<para>There are three types of bisection. The first one splits an interval in
as many equally wide sub-intervals as asked by the user. The second one splits
an interval along the points provided by the user.</para>

<programlisting><![CDATA[$ x in 6;               # split the range of "x" in six sub-intervals
$ x in (0.5,1.9999999); # split the range of "x" in three sub-intervals, the middle one being [0.5,~2]
$ x;                    # equivalent to: $ x in 4;]]></programlisting>

<para>The third kind of bisection tries to find by dichotomy the best range
split such that a goal of the logical formula holds true. This requires the
range of this goal to be specified, and the enclosed expression has to be
indicated on the left of the <code>$</code> symbol.</para>

<programlisting><![CDATA[{ x in [0,3] -> |z| <= 1b-26 }
|z| $ x;]]></programlisting>

<para>Contrarily to the first two kinds of bisection, this third one keeps the
range improvements only if the goal was finally reached. If there was a failure
in doing so, all the improvements are discarded. Gappa will display the
sub-range on which the goal was not proved. There is a failure when Gappa
cannot prove the goal on a range and this range cannot be split into two half
ranges, either because its internal precision is not enough anymore or because
the maximum depth of dichotomy has been reached. This depth can be set with the
<code>-Edichotomy=999</code> option.</para>

<para>More than one bisection hint can be used. And hints of the third kind can
try to satisfy more than one goal at once.</para>

<programlisting><![CDATA[a, b, c $ u;
a, d $ v;]]></programlisting>

<para>These two hints will be used one after the other. In particular, none
will be used when Gappa is doing a bisection with the other one. By adding
terms on the right of the <code>$</code> symbol, more complex hints can be
built. Beware of combinatorial explosions though. The following hint is an
example of a complex bisection: it asks Gappa to find a set of sub-ranges on
<code>u</code> such that the goal on <code>b</code> is satisfied when the range
on <code>v</code> is split into three sub-intervals.</para>

<programlisting><![CDATA[b $ u, v in 3]]></programlisting>

</section>
