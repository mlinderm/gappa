## Gappa++

Gappa++ is tool to help determining and verifying numerical behavior, and particularly rouding error in, computations with floating or fixed point operations. Gappa++ is an extension of [Gappa](http://gappa.gforge.inria.fr/) version 0.11.3, developed by [Guillame Melquiond](http://www.lri.fr/~melquion/).

Gappa++ is described in:

Linderman MD, Ho M, Dill DL, Meng TH, Nolan GP. [Towards program optimization through automated analysis of numerical precision](http://dx.doi.org/10.1145/1772954.1772987). Proc IEEE/ACM Intl Symp on Code Generation and Optimization (CGO); 2010. p. 230-7.

Gappa++ has been tested on Fedora and Centos-based systems. The MPFR and Boost 1.3.2+ libraries are required.

Gappa and Gappa++ are released under the terms of the GNU General Public License.
