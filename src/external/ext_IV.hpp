#ifndef GAPPA_EXTERNAL_EXTIV_H
#define GAPPA_EXTERNAL_EXTIV_H

#include <boost/numeric/interval.hpp>
#include "ext_num.hpp"
#include "aa_mpfr.hpp"

namespace EXT {

  /*---------------------------------------------------------------
   * IV
   * A custom interval type
   *--------------------------------------------------------------*/

  struct IV_Policies {
    struct rounding {
      static Num conv_down (int v)	    { return Num(v, GMP_RNDD); }
      static Num conv_up   (int v)	    { return Num(v, GMP_RNDU); }
      static Num conv_down (const Num &v) { return v; }
      static Num conv_up   (const Num &v) { return v; }
      static Num opp_down  (const Num &v) { return opp(v, GMP_RNDD); }
      static Num opp_up    (const Num &v) { return opp(v, GMP_RNDU); }
      static Num sqrt_down (const Num &v) { return sqrt(v, GMP_RNDD); }
      static Num sqrt_up   (const Num &v) { return sqrt(v, GMP_RNDU); }
      static Num log_down  (const Num &v) { return log(v, GMP_RNDD); }
      static Num log_up    (const Num &v) { return log(v, GMP_RNDU); }
      static Num exp_down  (const Num &v) { return exp(v, GMP_RNDD); }  
      static Num exp_up    (const Num &v) { return exp(v, GMP_RNDU); }  
      static Num log2_down (const Num &v) { return log2(v, GMP_RNDD); } 
      static Num log2_up   (const Num &v) { return log2(v, GMP_RNDU); } 
      static Num exp2_down (const Num &v) { return exp2(v, GMP_RNDD); } 
      static Num exp2_up   (const Num &v) { return exp2(v, GMP_RNDU); } 
      static Num add_down  (const Num &lhs, const Num &rhs) { return add(lhs, rhs, GMP_RNDD); } 
      static Num add_up    (const Num &lhs, const Num &rhs) { return add(lhs, rhs, GMP_RNDU); } 
      static Num sub_down  (const Num &lhs, const Num &rhs) { return sub(lhs, rhs, GMP_RNDD); } 
      static Num sub_up    (const Num &lhs, const Num &rhs) { return sub(lhs, rhs, GMP_RNDU); } 
      static Num mul_down  (const Num &lhs, const Num &rhs) { return mul(lhs, rhs, GMP_RNDD); } 
      static Num mul_up    (const Num &lhs, const Num &rhs) { return mul(lhs, rhs, GMP_RNDU); } 
      static Num div_down  (const Num &lhs, const Num &rhs) { return div(lhs, rhs, GMP_RNDD); } 
      static Num div_up    (const Num &lhs, const Num &rhs) { return div(lhs, rhs, GMP_RNDU); } 
      static Num median    (const Num &lo, const Num &hi) { return median(lo, hi); }
      
      typedef rounding unprotected_rounding;
    };
    struct checking {
      static bool is_nan(int) { return false; }
      static bool is_nan(double n) { return (n != n); }
      static bool is_nan(const Num &n) { return n.is_nan(); } 
      static Num empty_lower() { return Num(); }
      static Num empty_upper() { return Num(); }  
      static Num pos_inf() { return Num::pos_inf(); }
      static Num neg_inf() { return Num::neg_inf(); }
      static Num nan() { return Num(); }
      static bool is_empty(const Num &x, const Num &y) { return !(x <= y); }
    };
  };
  
  typedef boost::numeric::interval< Num, IV_Policies > IV;

  // Helpful operators

  bool is_zero(const IV& iv);

  void iv2mpfr(mpfr_t& lo, mpfr_t& hi, const IV& iv);

  AA::AAM operator+(const AA::AAM& lhs, const IV &rhs);

  std::ostream& operator<<(std::ostream&, const IV&);

} // End of namespace EXT

// Need to supply our own implementations for exp2 and log2

namespace boost {
  namespace numeric {
    
    template<class T, class Policies> inline
    interval<T, Policies> exp2(const interval<T, Policies>& x)
    {
      typedef interval<T, Policies> I;
      if (interval_lib::detail::test_input(x))
	return I::empty();
      typename Policies::rounding rnd;
      return I(rnd.exp2_down(x.lower()), rnd.exp2_up(x.upper()), true);
    }
    
    template<class T, class Policies> inline
    interval<T, Policies> log2(const interval<T, Policies>& x)
    {
      typedef interval<T, Policies> I;
      if (interval_lib::detail::test_input(x) ||
	  !interval_lib::user::is_pos(x.upper()))
	return I::empty();
      typename Policies::rounding rnd;
      typedef typename Policies::checking checking;
      T l = !interval_lib::user::is_pos(x.lower())
	? checking::neg_inf() : rnd.log2_down(x.lower());
      return I(l, rnd.log2_up(x.upper()), true);
    }
    
    template<class T, class Policies> inline
    interval<T, Policies> logp1(const interval<T, Policies>& x)
    {
      return log(x+ interval<T, Policies>(1));
    }

    template<class T, class Policies> inline
    interval<T, Policies> log2p1(const interval<T, Policies>& x)
    {
      return log2(x+ interval<T, Policies>(1));
    }

  } // end of namespace numeric
} // end of namespace boost

#endif
