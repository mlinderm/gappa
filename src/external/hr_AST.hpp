#ifndef GAPPA_EXTERNAL_HRAST_H
#define GAPPA_EXTERNAL_HRAST_H

#include <vector>
#include <boost/shared_ptr.hpp>

#include "ext_IV.hpp"
#include "aa_mpfr.hpp"

namespace EXT {
  namespace HR {
    
    class ExprValue;
    class ExprBuilder;

    namespace RW {
      class ExprRewriter;
    }

    class Expr {
    public:
      typedef unsigned int                 Idx_t;
      typedef boost::shared_ptr<ExprValue> Ptr_t;
      typedef std::pair<Idx_t,Ptr_t>       Edge_t;

      typedef std::vector<Edge_t>          Edges_t;

      typedef Edges_t::iterator            edge_iterator;
      typedef Edges_t::const_iterator      const_edge_iterator;

    private:     
      static Idx_t CurEdgeIdx_d;
      Edges_t      Exprs_d;

      Expr(ExprValue* expr) { Exprs_d.push_back(Edge_t(incIdx(),Ptr_t(expr))); }
      
      static inline Idx_t incIdx() { return CurEdgeIdx_d++; }
      static inline Idx_t curIdx() { return CurEdgeIdx_d; }

      const Expr& push_back(const Expr& expr) {
	for (const_edge_iterator i=expr.edge_begin(), e=expr.edge_end(); i!=e; ++i)
	  Exprs_d.push_back(Edge_t(incIdx(),i->second));
	return *this;
      }
      

    public:
      Expr()  {}
      Expr(const Expr& expr) : Exprs_d(expr.Exprs_d) {}
      ~Expr() {}
  
      // Accessors
      struct newest {
	static inline const Ptr_t& edge(const Expr& expr) { return expr.Exprs_d.back().second; }
      };

      ExprValue* newest() { return Exprs_d.back().second.get(); }
      const ExprValue* newest() const { return Exprs_d.back().second.get(); }

    

      edge_iterator edge_begin() { return Exprs_d.begin(); }
      edge_iterator edge_end() { return Exprs_d.end(); }
      
      const_edge_iterator edge_begin() const { return Exprs_d.begin(); }
      const_edge_iterator edge_end() const { return Exprs_d.end(); }

      
      static inline ExprValue* it2ast(edge_iterator& it) { return it->second.get(); }

      friend class ExprBuilder;
      friend class RW::ExprRewriter;
    };


    class ExprValue {
    public:
      typedef enum {
	EXPR_CONSTANT,
	EXPR_IDENT,
	EXPR_UNARYOP,
	EXPR_BINARYOP,
	EXPR_ROUNDOP,
	EXPR_AFFINE,
	EXPR_KIND_LAST
      } ExprKind_t;
      
    private:
      
      ExprKind_t Kind_d;
      
    public:
      
      ExprValue(ExprKind_t kind) : Kind_d(kind) {}
      
      ExprKind_t getKind() const { return Kind_d; }


      typedef Expr* child_iterator;
      typedef const Expr* const_child_iterator;
      
      virtual child_iterator child_begin() { return 0; }
      virtual child_iterator child_end() { return 0; }

      virtual const_child_iterator child_begin() const { return 0; }
      virtual const_child_iterator child_end() const { return 0; }

      template <typename To>
      static inline bool isa(const ExprValue* val) { return To::classof(val); }
      template <typename To>
      static inline bool isa(const Expr::Ptr_t& val) { return To::classof(val.get()); }

      template <typename To>
      static inline const To* dyn_cast(const ExprValue* val) {
	return ExprValue::isa<To>(val) ? static_cast<const To*>(val) : NULL;
      }
      template <typename To>
      static inline To* dyn_cast(ExprValue* val) {
	return ExprValue::isa<To>(val) ? static_cast<To*>(val) : NULL;
      }
      template <typename To>
      static inline To* dyn_cast(const Expr::Ptr_t& val) {
	return ExprValue::isa<To>(val) ? static_cast<To*>(val.get()) : NULL;
      }


      static inline bool classof(const ExprValue*) { return true; }
    };
    
    
    class ConstantExpr : public ExprValue {
    private:
      IV Val_d;

      ConstantExpr();
      
    public:
      ConstantExpr(const IV &iv) : ExprValue(ExprValue::EXPR_CONSTANT), Val_d(iv) {}
      ConstantExpr(const Num &lo, const Num &hi) : 
	ExprValue(ExprValue::EXPR_CONSTANT), Val_d(lo, hi) {}
      
      IV getVal() const { return Val_d; }
 
      static inline bool classof(const ExprValue* e) { return e->getKind() == ExprValue::EXPR_CONSTANT; }
    };
   
 
    class IdentExpr : public ExprValue {
    private:
      std::string Name_d;
      IV          Val_d;

      IdentExpr();
      
    public:
      IdentExpr(const std::string &name, const IV &iv) : 
	ExprValue(ExprValue::EXPR_IDENT), Name_d(name), Val_d(iv) {}
       
      const std::string& getName() const { return Name_d; }
      IV getVal() const { return Val_d; }

      static inline bool classof(const ExprValue* e) { return e->getKind() == ExprValue::EXPR_IDENT; }
    };


    class UnaryExpr : public ExprValue {
    public:
      enum Opcode {
	Log, Log2, Exp, Exp2, Logp1, Log2p1,
	END_OPCODE
      };

    private:
      Expr   SubExpr_d;
      
      Opcode Op_d; 
      
    private:

      UnaryExpr();
      
    public:
      UnaryExpr(Opcode op, const Expr& arg) :
	ExprValue(ExprValue::EXPR_UNARYOP), Op_d(op), SubExpr_d(arg) {}

      Opcode getOpcode() const { return Op_d; }
      const Expr& getOperand() const { return SubExpr_d; }

      child_iterator child_begin() { return &SubExpr_d; }
      child_iterator child_end() { return &SubExpr_d + 1; }
    
      static inline bool classof(const ExprValue* e) { return e->getKind() == ExprValue::EXPR_UNARYOP; }
    };


    class BinaryExpr : public ExprValue {
    public:
      // Opcodes in order of c99/algebraic precedence
      enum Opcode {
	Mul, Div, Add, Sub,
	END_OPCODE
      };

    private:
      enum { LHS, RHS, END_CHILD };
      Expr   SubExprs_d[END_CHILD];
      
      Opcode Op_d; 
      
    private:

      BinaryExpr();
      
    public:
      BinaryExpr(Opcode op, const Expr& lhs, const Expr& rhs) :
	ExprValue(ExprValue::EXPR_BINARYOP), Op_d(op) {
	SubExprs_d[LHS] = lhs;
	SubExprs_d[RHS] = rhs;
      }

      Opcode getOpcode() const { return Op_d; }

      const Expr& getLHS() const { return SubExprs_d[LHS]; }
      const Expr& getRHS() const { return SubExprs_d[RHS]; }

      child_iterator child_begin() { return SubExprs_d; }
      child_iterator child_end() { return &SubExprs_d[END_CHILD]; }

      static inline bool classof(const ExprValue* e) { return e->getKind() == ExprValue::EXPR_BINARYOP; }
    };    
    

    class RoundingExpr : public ExprValue {
    public:
      // Modes in precision order
      enum Mode {
	CUDA_32,
	IEEE_32,
	IEEE_64,
	x87_80,
	IEEE_128,
	FIXED
      };

      enum Direction {
	ROUND_UP,
	ROUND_DN,
	ROUND_ZR,
	ROUND_NE
      };

    private:
      Expr SubExpr_d;

      Mode      Mode_d;
      Direction Dir_d;
      int       Prec_d;  // Only relevant for fixed point

      IV Err_d;

    private:
      RoundingExpr();

    public:
      RoundingExpr(Mode mode, Direction dir, const IV& err, const Expr& arg) :
	ExprValue(ExprValue::EXPR_ROUNDOP), Mode_d(mode), Dir_d(dir), Err_d(err), SubExpr_d(arg) {
	assert(mode != FIXED);
      }
      RoundingExpr(Mode mode, Direction dir, int prec, const IV& err, const Expr& arg) :
	ExprValue(ExprValue::EXPR_ROUNDOP), Mode_d(mode), Dir_d(dir), Prec_d(prec), Err_d(err), SubExpr_d(arg) {
	assert(mode == FIXED);
      }

      Mode getMode() const { return Mode_d; }
      Direction getDirection() const { return Dir_d; }
      int getPrec() const; 

      const Expr& getOperand() const { return SubExpr_d; }
      IV getErr() const { return Err_d; }

      child_iterator child_begin() { return &SubExpr_d; }
      child_iterator child_end() { return &SubExpr_d + 1; }

      const_child_iterator child_begin() const { return &SubExpr_d; }
      const_child_iterator child_end() const { return &SubExpr_d + 1; }

      static inline bool classof(const ExprValue* e) { return e->getKind() == ExprValue::EXPR_ROUNDOP; }

      bool operator>=(const RoundingExpr& rhs) const;

    };

    class AffineExpr : public ExprValue {
    private:
      AA::AAM Val_d;

      AffineExpr();
      
    public:
      AffineExpr(const AA::AAM& aa) : 	ExprValue(ExprValue::EXPR_AFFINE), Val_d(aa) {}
      AffineExpr(const IV &iv) : 
	ExprValue(ExprValue::EXPR_AFFINE), 
	Val_d(iv.lower().getVal(),iv.upper().getVal(), parameter_internal_precision) {}
      AffineExpr(const Num &lo, const Num &hi) : 
	ExprValue(ExprValue::EXPR_AFFINE), 
	Val_d(lo.getVal(), hi.getVal(), parameter_internal_precision) {}
      
      AA::AAM getVal() const { return Val_d; }

      IV convert() const { 
	MPFR_DECL_INIT(lo, parameter_internal_precision);
	MPFR_DECL_INIT(hi, parameter_internal_precision);
	Val_d.convert(lo, hi);
	return IV(Num(lo), Num(hi));
      }
    
      static inline bool classof(const ExprValue* e) { return e->getKind() == ExprValue::EXPR_AFFINE; }
    };


    class ExprBuilder {
    public:
      // Basic expression constructors
      static Expr constantExpr(const IV& iv);
      static Expr constantExpr(int i);

      static Expr identExpr(const std::string& name, const IV& iv);

      // Rounding Expressions
      static Expr roundExpr(RoundingExpr::Mode mode, RoundingExpr::Direction dir, 
			    const IV& err, const Expr& arg);
      static Expr roundExpr(RoundingExpr::Mode mode, RoundingExpr::Direction dir, int prec, 
			    const IV& err, const Expr& arg);
      
      // Unary Expressions
      static Expr logExpr(const Expr& arg);
      static Expr log2Expr(const Expr& arg);
      static Expr expExpr(const Expr& arg);
      static Expr exp2Expr(const Expr& arg);
      static Expr logp1Expr(const Expr& arg);
      static Expr log2p1Expr(const Expr& arg);

      // Binary Expressions
      static Expr mulExpr(const Expr& lhs, const Expr& rhs);
      static Expr divExpr(const Expr& lhs, const Expr& rhs);
      static Expr addExpr(const Expr& lhs, const Expr& rhs);
      static Expr subExpr(const Expr& lhs, const Expr& rhs);
      

      // Affine Expressions
      static Expr affineExpr(const IV& iv);
      static Expr affineExpr(const AA::AAM& aa);
    };


    template<typename ImplClass, typename RetTy=void>
    class ExprVisitor {
      
#define DISPATCH(NAME, CLASS)						\
      return static_cast<ImplClass*>(this)->Visit ## NAME(static_cast<CLASS*>(expr))
#define FALLBACK(NAME, CLASS, PARENT)						\
      virtual RetTy Visit ## NAME(CLASS* expr) { return this->Visit ## PARENT(expr); }

      

    public:
      RetTy Visit(ExprValue* expr) {
	switch(expr->getKind()) {
	default: DISPATCH(Expr, ExprValue); 
	case ExprValue::EXPR_CONSTANT: DISPATCH(ConstantExpr, ConstantExpr);
	case ExprValue::EXPR_IDENT:    DISPATCH(IdentExpr, IdentExpr);
	case ExprValue::EXPR_UNARYOP:  DISPATCH(UnaryExpr, UnaryExpr);
	case ExprValue::EXPR_BINARYOP: DISPATCH(BinaryExpr, BinaryExpr);
	case ExprValue::EXPR_ROUNDOP:  DISPATCH(RoundingExpr, RoundingExpr);
	case ExprValue::EXPR_AFFINE:   DISPATCH(AffineExpr, AffineExpr);
	}
      }

      inline void VisitBase(Expr& expr) {
	typedef Expr::edge_iterator iter_t;
	for (iter_t i=expr.edge_begin(), e=expr.edge_end(); i!=e; ++i)
	  this->Visit(Expr::it2ast(i)); 
      }
      
      virtual RetTy Visit(Expr& expr) = 0;

      // Base Case
      virtual RetTy VisitExpr(ExprValue *expr) { return RetTy(); }

      FALLBACK(ConstantExpr, ConstantExpr, Expr)
      FALLBACK(IdentExpr, IdentExpr, Expr)
      FALLBACK(UnaryExpr, UnaryExpr, Expr)
      FALLBACK(BinaryExpr, BinaryExpr, Expr)
      FALLBACK(RoundingExpr, RoundingExpr, Expr)
      FALLBACK(AffineExpr, AffineExpr, Expr)

#undef DISPATCH
#undef FALLBACK

    };

   


  } // end of HR namespace
} // end of EXT namespace

#endif
