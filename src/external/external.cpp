#include <cassert>

#include "numbers/interval.hpp"
#include "numbers/interval_utility.hpp"
#include "proofs/schemes.hpp"
#include "parser/ast.hpp"

#include "external.hpp"


External_Scheme::External_Scheme(External &ext_tool, predicated_real const &p)
  : proof_scheme(p), ExtTool(ext_tool) {

  ExtTool.fillNeededReals(Needed,real.real());

}
  

node*
External_Scheme::generate_proof() const {
  
  // Fill in hypotheses
  std::vector<property> hyps(Needed.size());
  if (!fill_hypotheses(&hyps[0],Needed)) return NULL;

  // Invoke external tool
  interval res = ExtTool.compute(real.real(), hyps);
  if (!is_defined(res))
    return NULL;
  
  return create_theorem(hyps.size(), &hyps[0], property(real, res),
			"External");
}



preal_vect 
External_Scheme::needed_reals() const {
  return Needed;
}


proof_scheme* 
ExternalSelect_Factory::operator()(predicated_real const &src, 
				   ast_real_vect const &holders) const {
  if (ExtTool.canCreateScheme(src))
    return new External_Scheme(ExtTool,src);
  else
    return NULL;
}



// Interface to parser for external tool rules
extern int register_user_affine_tool(ast_real const *);
extern int register_user_hybrid_tool(ast_real const *);

int 
register_user_external(ast_real const *r, ast_ident *t) {
  if (t->name.compare("affine") == 0) {
    return register_user_affine_tool(r);
  } else if (t->name.compare("hybrid") == 0) {
    return register_user_hybrid_tool(r);
  } else {
    // Unknown external tool
    return 1;
  }
}
