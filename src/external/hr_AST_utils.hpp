#ifndef GAPPA_EXTERNAL_HRASTUTILS_H
#define GAPPA_EXTERNAL_HRASTUTILS_H

#include <ostream>
#include <iomanip>

#include "hr_AST.hpp"

namespace EXT {
  namespace HR {

    class Expr2IV : public ExprVisitor<Expr2IV,IV> {
    public:

      // Visit the "newest" edge
      IV Visit(Expr& expr) { return ExprVisitor<Expr2IV,IV>::Visit(expr.newest()); }
      IV Visit(const Expr& expr) { return ExprVisitor<Expr2IV,IV>::Visit((ExprValue*)expr.newest()); }

      IV VisitConstantExpr(ConstantExpr *expr) { return expr->getVal(); }
      IV VisitIdentExpr(IdentExpr *expr) { return expr->getVal(); }

      IV VisitUnaryExpr(UnaryExpr *expr) {
	switch (expr->getOpcode()) {
	default: assert(false); return IV();
#define CASE(OPCODE, OPFUNC) case UnaryExpr::OPCODE: return OPFUNC(Visit(expr->getOperand()));
	  CASE(Log,log);
	  CASE(Log2,log2);
	  CASE(Exp,exp);
	  CASE(Exp2,exp2);
	  CASE(Logp1,logp1);
	  CASE(Log2p1,log2p1);
#undef CASE	  
	}
      }

      IV VisitBinaryExpr(BinaryExpr *expr) {
	switch (expr->getOpcode()) {
	default: assert(false); return IV();
#define CASE(OPCODE, OPFUNC) case BinaryExpr::OPCODE: return (Visit(expr->getLHS()) OPFUNC Visit(expr->getRHS()));
	  CASE(Mul, *);
	  CASE(Div, /);
	  CASE(Add, +);
	  CASE(Sub, -);
#undef CASE
	}
      }

      IV VisitRoundingExpr(RoundingExpr *expr) {
	return (Visit(expr->getOperand()) + expr->getErr());
      }
      
      IV VisitAffineExpr(AffineExpr *expr) { return expr->convert(); }

      IV VisitExpr(ExprValue *expr) {
	assert(false && "AST kind not yet supported");
	return IV();
      }
    };



    class ExprPrinter : public ExprVisitor<ExprPrinter> {
    private:
      std::ostream &OS_d;
      int Indent_d;

      void indent() { for (int i=0; i<Indent_d; i++) OS_d << "   "; }

    public:
      ExprPrinter(std::ostream& os) : OS_d(os), Indent_d(0) {}

      void Visit(Expr& expr) { 
      	//int edge = 0;
	//typedef Expr::edge_iterator iter_t;
	//for (iter_t i=expr.edge_begin(), e=expr.edge_end(); i!=e; ++i) {
	//  OS_d << edge++;
	//  ExprVisitor<ExprPrinter>::Visit(Expr::it2ast(i));
	//}
	ExprVisitor<ExprPrinter>::Visit(Expr::newest::edge(expr).get());
      }

      void VisitBinaryExpr(BinaryExpr *expr) {
	indent();
	OS_d << "BinaryExpr: ";
	switch (expr->getOpcode()) {
	default: OS_d << "Invalid Opcode"; break;
#define CASE(OPCODE) case BinaryExpr::OPCODE: OS_d << #OPCODE; break;
	  CASE(Mul);
	  CASE(Div);
	  CASE(Add);
	  CASE(Sub);
#undef CASE
	}
	OS_d << " Value: " << Expr2IV().VisitBinaryExpr(expr) << std::endl;
	
	Indent_d++;
	for (ExprValue::child_iterator i=expr->child_begin(), e=expr->child_end(); i!=e; ++i)
	  Visit(*i);
	Indent_d--;
      }

      void VisitRoundingExpr(RoundingExpr *expr) {
	indent();
	OS_d << "RoundingExpr: ";
	switch (expr->getMode()) {
	case RoundingExpr::IEEE_32:  OS_d << "float<ieee_32,"; break;
	case RoundingExpr::IEEE_64:  OS_d << "float<ieee_64,"; break;
	case RoundingExpr::IEEE_128: OS_d << "float<ieee_128,"; break;
	case RoundingExpr::x87_80:   OS_d << "float<x87_80,"; break;
	case RoundingExpr::CUDA_32:  OS_d << "float<cuda_32,"; break;
	case RoundingExpr::FIXED:    OS_d << "fixed<" << expr->getPrec(); break;
	}
	switch (expr->getDirection()) {
	case RoundingExpr::ROUND_UP: OS_d << "up>"; break;
	case RoundingExpr::ROUND_DN: OS_d << "dn>"; break;
	case RoundingExpr::ROUND_ZR: OS_d << "zr>"; break;
	case RoundingExpr::ROUND_NE: OS_d << "ne>"; break;
	}
	OS_d << " Err: " << expr->getErr() << " Value: " << Expr2IV().VisitRoundingExpr(expr) << std::endl;
	Indent_d++;
	for (ExprValue::child_iterator i=expr->child_begin(), e=expr->child_end(); i!=e; ++i)
	  Visit(*i);
	Indent_d--;
      }

      void VisitUnaryExpr(UnaryExpr *expr) {
	indent();
	OS_d << "UnaryExpr: ";
	switch (expr->getOpcode()) {
	default: OS_d << "invalid opcode"; break;
#define CASE(OPCODE) case UnaryExpr::OPCODE: OS_d << #OPCODE; break;
	  CASE(Log);
	  CASE(Log2);
	  CASE(Exp);
	  CASE(Exp2);
	  CASE(Logp1);
	  CASE(Log2p1);
#undef CASE
	}
	OS_d << " Value: " << Expr2IV().VisitUnaryExpr(expr) << std::endl;
	Indent_d++;
	for (ExprValue::child_iterator i=expr->child_begin(), e=expr->child_end(); i!=e; ++i)
	  Visit(*i);
	Indent_d--;
      }

      void VisitIdentExpr(IdentExpr *expr) {
	indent();
	OS_d << "Ident Expr: " << expr->getName() << " " << expr->getVal() << std::endl;
      }
	
      void VisitConstantExpr(ConstantExpr *expr) {
	indent();
	OS_d << "Constant Expr: " << expr->getVal() << std::endl;
      }
  
      void VisitAffineExpr(AffineExpr *expr) {
	indent();
	OS_d << "Affine Expr: " << Expr2IV().VisitAffineExpr(expr) << std::endl;
      }

    };


  } // end of HR namespace
} // end of EXT namespace


#endif
