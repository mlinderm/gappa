#ifndef GAPPA_EXTERNAL_EXTNUM_H
#define GAPPA_EXTERNAL_EXTNUM_H

#include <mpfr.h>
#include <boost/shared_ptr.hpp>

// Precision used within Gappa
extern int parameter_internal_precision;

namespace EXT {
  
  // Arbitrary Precision Number
  class Num {
  private:
     
    struct NumVal {
      mpfr_t Val_d;
      
      NumVal() { mpfr_init2(Val_d, parameter_internal_precision); }
      NumVal(const mpfr_t val) { mpfr_init2(Val_d, parameter_internal_precision); mpfr_set(Val_d, val, GMP_RNDN); }
      ~NumVal() { mpfr_clear(Val_d); }
      
      static NumVal *empty_NumVal();
      static NumVal *pos_inf_NumVal();
      static NumVal *neg_inf_NumVal();
    };
    
  private:
    
    boost::shared_ptr<NumVal> Num_d;
    
    Num(NumVal* v) : Num_d(v) {}
    
    static inline mpfr_t& n2mpfr(const Num& n) { return n.Num_d->Val_d; }

  public:
    Num();
    Num(const Num& num) : Num_d(num.Num_d) {}
    Num(const mpfr_t num) : Num_d(new NumVal(num)) {}
    Num(int num, mp_rnd_t rnd=GMP_RNDN) { Num_d.reset(new NumVal()); mpfr_set_si(Num_d->Val_d, num, rnd); }
    
    mpfr_t const& getVal() const { return Num_d->Val_d; }

    Num& operator=(const Num& rhs) { Num_d = rhs.Num_d; return *this; }
    bool operator==(const Num& rhs) const { return (mpfr_cmp(Num_d->Val_d,rhs.Num_d->Val_d) == 0); }
    bool operator<(const Num& rhs) const { return (mpfr_cmp(Num_d->Val_d,rhs.Num_d->Val_d) < 0); }
    bool operator>(const Num& rhs) const { return (mpfr_cmp(Num_d->Val_d,rhs.Num_d->Val_d) > 0); }
    bool operator<=(const Num& rhs) const { return (mpfr_cmp(Num_d->Val_d,rhs.Num_d->Val_d) <= 0); }
    bool operator>=(const Num& rhs) const { return (mpfr_cmp(Num_d->Val_d,rhs.Num_d->Val_d) >= 0); }

    Num operator-() const { return opp(*this); }

    bool is_nan() const { return mpfr_nan_p(Num_d->Val_d); }

    static Num nan(); 
    static Num pos_inf();
    static Num neg_inf();

    friend Num opp (const Num &x, mp_rnd_t rnd=GMP_RNDN);
    friend Num sqrt(const Num &x, mp_rnd_t rnd=GMP_RNDN);
    friend Num exp (const Num &x, mp_rnd_t rnd=GMP_RNDN);
    friend Num log (const Num &x, mp_rnd_t rnd=GMP_RNDN);
    friend Num exp2(const Num &x, mp_rnd_t rnd=GMP_RNDN);
    friend Num log2(const Num &x, mp_rnd_t rnd=GMP_RNDN);

    friend Num add(const Num &lhs, const Num &rhs, mp_rnd_t rnd=GMP_RNDN);
    friend Num sub(const Num &lhs, const Num &rhs, mp_rnd_t rnd=GMP_RNDN);
    friend Num mul(const Num &lhs, const Num &rhs, mp_rnd_t rnd=GMP_RNDN);
    friend Num div(const Num &lhs, const Num &rhs, mp_rnd_t rnd=GMP_RNDN);

    friend Num median(const Num &lo, const Num &hi);

    friend std::ostream& operator<<(std::ostream&, const Num&);
   };

  Num opp (const Num &x, mp_rnd_t rnd);
  Num sqrt(const Num &x, mp_rnd_t rnd);
  Num exp (const Num &x, mp_rnd_t rnd);
  Num log (const Num &x, mp_rnd_t rnd);
  Num exp2(const Num &x, mp_rnd_t rnd);
  Num log2(const Num &x, mp_rnd_t rnd);
  
  Num add(const Num &lhs, const Num &rhs, mp_rnd_t rnd);
  Num sub(const Num &lhs, const Num &rhs, mp_rnd_t rnd);
  Num mul(const Num &lhs, const Num &rhs, mp_rnd_t rnd);
  Num div(const Num &lhs, const Num &rhs, mp_rnd_t rnd);

  Num median(const Num &lo, const Num &hi);

  std::ostream& operator<<(std::ostream&, const Num&);

} // end of namespace EXT



#endif
