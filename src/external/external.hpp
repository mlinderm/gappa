#ifndef GAPPA_EXTERNAL_EXTERNAL_HPP
#define GAPPA_EXTERNAL_EXTERNAL_HPP

#include <vector>
#include <cassert>

#include "proofs/schemes.hpp"

// Forward declarations
struct ast_real;
struct ast_indet;
struct property;


// Interface to parser for user controlled external tool
// invocation
int register_user_external(ast_real const *, ast_ident *);

// Base class for all External tools
class External {
public:
  virtual ~External() {}

  virtual interval compute(const ast_real *target, const std::vector<property> &hyps) = 0;

  virtual bool canCreateScheme(predicated_real const &target) = 0;

  virtual void fillNeededReals(preal_vect &needed, const ast_real *target) = 0;
};


class External_Scheme : public proof_scheme {
private:
  External &ExtTool;
  preal_vect Needed;

public:
  
  External_Scheme(External &ext_tool, predicated_real const &p); 

  node *generate_proof() const;

  preal_vect needed_reals() const;
};

// ExternalSelect (External *)
//
// Creates external proof scheme for expressions selected by external
// tool. Allows external tool to filter expressions.

class ExternalSelect_Factory : public scheme_factory {
private:
  External &ExtTool;

public:

  ExternalSelect_Factory(External &ext_tool, predicated_real const &p) 
    : scheme_factory(p), ExtTool(ext_tool) {}

  proof_scheme* operator()(predicated_real const &src, 
			   ast_real_vect const &holders) const;

  bool isExternal() const { return true; }
};


#endif
